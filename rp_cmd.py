import time
from rp_lib import * # Bibliothèque Ropy

###############################################################################
# rp_cmd-microbit.py
# @title: Commandes pour le Rover Ropy
# @project: Ropy (Blender-EduTech)
###############################################################################

###############################################################################
# Initialisation du niveau :
# Niveau 1 : Les premiers pas de Ropy
# Niveau 2 : Ma première fonction
# Niveau 3 : Sécuriser Ropy
# Niveau 4 : Partir au bout du monde
# Niveau 5 : Faire face à l'inconnu
# Niveau 6 : Se rendre utile
###############################################################################

###############################################################################
# Fonctions
###############################################################################

###############################################################################
# Commandes
###############################################################################

def commandes():

    # Ecrire votre code ici ...
    print ('okok')
    rp_gauche()
    rp_avancer()
    rp_avancer()
    rp_droite()
    for i in range (10):
        rp_avancer()
        rp_marquer()
    
    rp_fin() # A garder

###############################################################################
# En: External call << DONT CHANGE THIS SECTION >>
# Fr: Appel externe << NE PAS MODIFIER CETTE SECTION >>
###############################################################################

if __name__=='start':
    thread_cmd_start(commandes)
if __name__=='stop':
    thread_cmd_stop()
