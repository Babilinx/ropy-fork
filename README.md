# Ropy
**Discover Python with Ropy (coding adventure)**<br/>
**Découvre le Python avec Ropy (code ton aventure)**

Ropy est un rover martien qui se programme en Python. Les aventures de Ropy permettent la découverte de la programmation procédurale et du language Python. A
travers les différents missions, donc de manière graduée, les élèves vont apprendre à manipuler les structures algorithmiques et à les coder en Python.

[![Ropy présentation](img/ropy-v2-presentation.png)](https://tube-sciences-technologies.apps.education.fr/w/b11be555-bd05-462b-8ff6-177ec18c1830)

![Poster](img/poster.png)

Ropy a fortement été inpiré par [RobotProg](http://www.physicsbox.com/indexrobotprogfr.html) et [Light Bot](https://lightbot.com/).

Ce jeu sérieux fait partie du projet open source [Blender-EduTech (Blender/UPBGE pour l'Enseignement Technologique)](https://gitlab.com/blender-edutech).

## Téléchargement

Les binaires (Game Engine Runtime) sont hébergés sur [phroy.org](http://www.phroy.org/cloud/index.php/s/6K2g6sPLAoK8rj5).

## Instructions et missions

Le script Python qui permet la commande du robot est le fichier **'rp_cmd.py'**. Il est éditable avec tout éditeur (Spyder, Emacs, Atom, ...).

Les instructions de base sont : 
- Avancer : **rp_avancer()**
- Reculer : **rp_reculer()**	
- Tourner à gauche : **rp_gauche()**
- Tourner à droite : **rp_droite()**
- Marquer la case : **rp_marquer()**
- Détecter la présence d'un mur devant à un pas : **rp_detect()** -> retourne True en présence d'un mur et False en absence de mur

Il y a 6 missions :
- Mission 1 : Les premiers pas de Ropy
- Mission 2 : Ma première fonction
- Mission 3 : Sécuriser Ropy
- Mission 4 : Partir au bout du monde
- Mission 5 : Faire face à l'inconnu
- Mission 6 : Se rendre utile

Du niveau 1 au niveau 5, chaque niveau donne lieu à la découverte d'une nouvelle structure algorithmique. Le niveau 6 correspond à la mission principale,
il faudra mobiliser l'ensemble de structures vues précédement.

Par son interface sur le port série, Ropy peut devenir le jumeau numérique d'un robot réel. Trois missions sont proposées sur son **jumelage avec le robot
Maqueen** (carte micro:bit) présentes dans les documents pédagogiques.

Deux vidéos :
- [Mise en place et présentation (version longue) de Ropy](https://tube-sciences-technologies.apps.education.fr/w/aGrLM2tkG356hMQWzsvqtd),
- [Jumelage de Ropy avec Maqueen](https://tube-sciences-technologies.apps.education.fr/w/2cekcqLFWgLHWidYPS4ZFb).

## Documents pédagogiques

Les applications pédagogique se trouvent dans le [dépôt des documents pédagogiques du projet Blender-EduTech](https://gitlab.com/blender-edutech/blender-edutech-oer-french) .

Ropy est une plateforme pédagogique où d'autres missions pouvent être dévelopées pour mettre en oeuvre des concepts comme la programmation objet, le
multithreading ou encore le machine learning.

## Développement

L'environnement de développement est basé sur : la plateforme de modélisation et d'animation 3D Blender ( https://blender.org ), le langage Python
(https://python.org ) et le moteur de jeu 3D UPBGE ( https://upbge.org ).

Le code source, les fichiers blender et les assets sont hébergés sur le dépôt [gitlab](https://gitlab.com/blender-edutech/ropy).

Les bibliothèques suivantes ne sont pas incluses par défaut dans l'environnement UPBGE : 
- [**Pylint**](https://pylint.pycqa.org) : vérificateur du code Python
- [**pySerial**](https://pyserial.readthedocs.io) : communication sur le port série

Il faut donc les installer localement (dans UPBGE), les étapes sont :
- **GNU/Linux** : La configuration ici présente est UPBGE installé sur ~ avec Python 3.9 :
	- Aller dans le répertoire local de Python de UPBGE: $ cd ~/UPBGE-0.30-linux-x86_64/3.0/python/bin
	- Installer le gestionnaire de paquet pip : $ ./python3.9 -m ensurepip --default-pip
	- Installer Pylint : $ ./pip install pylint -t ~/UPBGE-0.30-linux-x86_64/3.0/python/lib/python3.9/site-packages
	- Installer pySerial : $ ./pip install pyserial -t ~/UPBGE-0.30-linux-x86_64/3.0/python/lib/python3.9/site-packages

- **Windows** : La configuration ici présente est UPBGE installé sur le bureau utilisateur (prenom.nom) avec la distribution Anaconda installée :
	- Avec Anaconda Navigator ouvrir un terminal Powershell
	- Installer Pylint : pip install pylint -t C:\Users\prenom.nom\Desktop\UPBGE-0.30-windows-x86_64\3.0\python\lib\site-packages
	- Installer pySerial : pip install pyserial -t C:\Users\prenom.nom\Desktop\UPBGE-0.30-windows-x86_64\3.0\python\lib\site-packages
