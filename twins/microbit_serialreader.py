from microbit import uart, sleep
from microbit import *

###############################################################################
# microbit-serialreader.py
# @title: Lecteur du port serie d'une carte micro:bit
# @project: Ropy (Blender-EduTech)
###############################################################################

uart.init(baudrate= 115200) # Initialisation du port série

while True:

    while not uart.any(): # Attente d'un message

        # Envoi d'un message
        if button_a.is_pressed() :
            display.scroll("-> A")
            uart.write("A\n")
        if button_b.is_pressed() :
            display.scroll("-> B")
            uart.write("B\n")
        # pass

    # Réception d'un message
    msg = uart.readline()
    display.scroll(msg[:-1]) # Affichage du message sans le '/n'
    # display.scroll(str(msg[0])+" "+str(msg[1])) # Affichage du message en code ASCII
    sleep(400)
