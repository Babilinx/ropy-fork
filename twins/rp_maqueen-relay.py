from microbit import uart, sleep
from microbit import *
import radio

###############################################################################
# rp_maqueen-relay.py
# @title: Jumeau Maqueen : Programme de la carte microbit du relais
# @project: Ropy (Blender-EduTech)
# @lang: fr
# @authors: Philippe Roy <philippe.roy@ac-grenoble.fr>
# @copyright: Copyright (C) 2022 Philippe Roy
# @license: GNU GPL
###############################################################################

###############################################################################
# Initialisation
###############################################################################

attente_image = Image("00000:00000:00300:00000:00000")
display.show(attente_image)  # Témoin de fonctionnement
balise_image = Image("33333:36663:36963:36663:33333")
radio.config(group=1, queue=4, length=8)
radio.on()

uart.init(baudrate= 115200) # Initialisation du port série

###############################################################################
# Écoute du port série et de la radio pour relayage
###############################################################################

temps_avancer=10
temps_tourner=10
temps_marquer=100
temps_objectif=100
temps_forer=100
temps_colision=100

while True:
 
    while not uart.any(): # Attente d'un message du port série
        
        if button_a.is_pressed() or button_b.is_pressed(): # A propos
            display.scroll ("Ropy : Relais")
            display.show(attente_image) # Témoin de fonctionnement
        # pass

        ordre_radio=radio.receive()

        ##########################
        # Transmission d'un ordre radio (venant de la télécommande -> série)
        ##########################

        # Avancer : AV
        if ordre_radio=="AV":
            display.show(Image.ARROW_N)
            uart.write("AV\n")

        # Reculer : RE
        if ordre_radio=="RE":
            display.show(Image.ARROW_S)
            uart.write("RE\n")

        # Gauche : GA
        if ordre_radio=="GA":
            display.show(Image.ARROW_W)
            uart.write("GA\n")

        # Droite : DR
        if ordre_radio=="DR":
            display.show(Image.ARROW_E)
            uart.write("DR\n")

        # Marquer : MA
        if ordre_radio=="MA":
            display.show(balise_image)
            uart.write("MA\n")

        # Stop : ST (arrêt de la télécommande)
        if ordre_radio=="ST":
            display.show(Image.NO)
            uart.write("ST\n")

        # Fin : FI (fin du mouvement)
        if ordre_radio=="FI":
            display.show(attente_image)  # Témoin de fonctionnement

    ##########################
    # Transmission d'un ordre série (venant du jumeau numérique -> radio)
    ##########################

    display.show(' ') # Sinon bug ?
    ordre_serie = uart.readline()

    # Avancer : AV
    if ordre_serie[0] ==65 and ordre_serie[1] ==86 : # Code ASCII : 65 (A) puis 86 (V)
        display.show(Image.ARROW_N)
        radio.send("AV")
        sleep(temps_avancer)

    # Reculer : RE
    if ordre_serie[0] ==82 and ordre_serie[1] ==69 : # Code ASCII : 82 (R) puis 69 (E)
        display.show(Image.ARROW_S)
        radio.send("RE")
        sleep(temps_avancer)

    # Gauche : GA
    if ordre_serie[0] ==71 and ordre_serie[1] ==65 : # Code ASCII : 71 (G) puis 65 (A)
        display.show(Image.ARROW_W)
        radio.send("GA")
        sleep(temps_tourner)

    # Droite : DR
    if ordre_serie[0] ==68 and ordre_serie[1] ==82 : # Code ASCII : 68 (D) puis 82 (R)
        display.show(Image.ARROW_E)
        radio.send("DR")
        sleep(temps_tourner)

    # Marquer : MA
    if ordre_serie[0] ==77 and ordre_serie[1] ==65 : #  Code ASCII : 77 (M) puis 65 (A)
        display.show(balise_image)
        radio.send("MA")
        sleep(temps_marquer)

    # Objectif atteint : OB
    if ordre_serie[0] ==79 and ordre_serie[1] ==66 : #  Code ASCII : 79 (O) puis 66 (B)
        display.show(Image.HAPPY)
        radio.send("OB")
        sleep(temps_objectif)
        display.show(attente_image) # Témoin de fonctionnement

    # Forer : FO
    if ordre_serie[0] ==70 and ordre_serie[1] ==79 : #  Code ASCII : 70 (F) puis 79 (O)
        display.show(Image.DIAMOND)
        radio.send("FO")
        sleep(temps_forer)

    # Colision : CO
    if ordre_serie[0] ==67 and ordre_serie[1] ==79 : #  Code ASCII : 67 (C) puis 79 (O)
        display.show(Image.SKULL)
        radio.send("CO")
        sleep(temps_colision)

    # Configuration : CF
    # FIXME : bugé 
    
    if ordre_serie[0] ==67 and ordre_serie[1] ==70 : #  Code ASCII : 67 (C) puis 70 (F)
        display.scroll ("CF")
        radio.send("CF")

        # Boucle d'écoute spécifique à la configuration
        while True:
            while not uart.any(): # Attente d'un message
                pass
            display.show(' ')
            ordre_serie = uart.readline()
            if ordre_serie[0] ==70 and ordre_serie[1] ==67 : #  Code ASCII : 70 (F) puis 67 (C) -> fin de la configuration
                break
            # display.scroll (ordre_serie[:-1])
            radio.send(str(ordre_serie[:-1]))
        # display.scroll ("FC")
        radio.send("FC")

    # Fin : FI
    if ordre_serie[0] ==70 and ordre_serie[1] ==73 : #  Code ASCII : 70 (F) puis 73 (I)
        radio.send("FI")
        display.show(attente_image)  # Témoin de fonctionnement

    sleep(400)
