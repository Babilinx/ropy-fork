#include "Wire.h"

/******************************************************************************
 * arduino-serialreader.ino
 * @title: Lecteur du port serie d'une carte arduino
 * @project: Ropy (Blender-EduTech)
 * @lang: fr
 * @authors: Philippe Roy <philippe.roy@ac-grenoble.fr>
 * @copyright: Copyright (C) 2022 Philippe Roy 
 * @license: GNU GPL
 ******************************************************************************/

/******************************************************************************
 * Pupitre
 ******************************************************************************/
 
 // Adressage Led Arduino
const int led_com =  13; // Led de communication modele 3d-> arduino (onboard)

// Adressage Entrees Arduino
const int bt =  4; // Bouton

// Entrees numeriques (modele 3D) 
bool bt_num=false; // Bouton numérique

// Adressage Sorties Arduino
const int v = 2; // Voyant

/******************************************************************************
 * Communication serie
 ******************************************************************************/

String serial_msg = ""; // Message 
bool serial_msg_complet = false;  // Flag de message complet

/******************************************************************************
 * Initialisation
 ******************************************************************************/
   
void setup() {
    
    pinMode(bt, INPUT); // Bouton
    pinMode(v, OUTPUT); // Voyant
    digitalWrite(v, LOW);

    Serial.begin(115200);   // Liaison série
}

/******************************************************************************
 * Boucle principale
 ******************************************************************************/

void loop() {

    /*****
     * Communication : modele 3d -> arduino
     *****/
     
    // digitalWrite(v, HIGH);
  
    if (serial_msg_complet) {
      Serial.println(serial_msg); // Echo
      if (serial_msg =="S\n") bt_num=true; // S pour Set
	    if (serial_msg =="R\n") bt_num=false; // R pour Reset
 
	
	// Serial.println("Echo : "+serial_msg);
    	serial_msg = "";
    	serial_msg_complet = false;
    }
     
    /*****
     * Bouton (arduino -> modele 3d)
     *
     * Bouton physique : LOW = actif et HIGH = pas actif
     *****/

    //Serial.println("Bouton"+ digitalRead(bt));
    if (digitalRead(bt)== LOW) {
        Serial.println("Bouton actif");
        digitalWrite(v, HIGH);
        delay(300);
    }
    else {
        // Serial.println("Bouton repos");
        digitalWrite(v, LOW);
    }
   

    /*****
     * Voyant modele 3d-> arduino
     *
     * Bouton numérique (modele 3d) : true = actif et false = pas actif
     *****/

    if (bt_num) {
        digitalWrite(v, HIGH);
    }
    else {
        digitalWrite(v, LOW);
    }

    //delay(300);
}

/******************************************************************************
 * Evenements provoques par la communication serie
 ******************************************************************************/

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    serial_msg += inChar;
    if (inChar == '\n') {
	serial_msg_complet = true;
        digitalWrite(v, HIGH);
    }
  }
}
