import bge # Bibliothèque Blender Game Engine (UPBGE)
import time
from rp_lib import * # Bibliothèque Ropy

###############################################################################
# rp_cmd-microbit.py
# @title: Exemple pour le lecteur du port série Micro:bit
# @project: Ropy (Blender-EduTech)
###############################################################################

###############################################################################
# Initialisation du niveau :
# Niveau 1 : Les premiers pas de Ropy
# Niveau 2 : Ma première fonction
# Niveau 3 : Sécuriser Ropy
# Niveau 4 : Partir au bout du monde
# Niveau 5 : Faire face à l'inconnu
# Niveau 6 : Se rendre utile
###############################################################################

###############################################################################
# Fonctions
###############################################################################

###############################################################################
# Commandes
###############################################################################

def commandes():

    # rp_serie_ports() # Affichage de la liste des ports série
    rp_jumeau() # Vitesse 115200 baud

    rp_serie_msg("Press a button !") # Envoyer un message

    print (rp_serie_rcpt()) # Réception d'un message (bouton appuyé)
 
    rp_fin() # A garder

###############################################################################
# En: Externals calls << DONT CHANGE THIS SECTION >>
# Fr: Appels externes << NE PAS MODIFIER CETTE SECTION >>
###############################################################################

if __name__=='start':
    thread_cmd_start(commandes)
if __name__=='stop':
    thread_cmd_stop()
