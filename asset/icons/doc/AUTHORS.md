Icons by Game-icons.net

Authors :
- Delapouite < delapouite@gmail.com > : https://delapouite.com
- Lorc : https://lorcblog.blogspot.com
- Contributors : https://game-icons.net/about.html#authors

Licence : Creative Commons BY 3.0

Website : www.game-icons.net

Source repository : www.github.com/game-icons
