import bge  # Blender Game Engine (UPBGE)
import bpy  # Blender
import aud  # Sounds
import math
import mathutils
import numpy as np
import time
import sys
import os
import webbrowser # Lien internet
import threading # Multithreading
import xml.etree.ElementTree as ET # Creating/parsing XML file
import runpy # Exécution de script Python légère (sans import)
from pylint import epylint as lint # Mesure de la qualité d'un code Python

import rp_map1 as rp_map # Map definition
import rp_doc # Documentation
import rp_store # Store
import rp_about # About

###############################################################################
# rp.py
# @title: Ropy
# @project: Ropy (Blender-EduTech)
# @lang: fr
# @authors: Philippe Roy <philippe.roy@ac-grenoble.fr>
# @copyright: Copyright (C) 2020-2022 Philippe Roy
# @license: GNU GPL
# 
# Ropy est destiné à la découverte de la programmation procédurale et du language Python.
# A travers plusieurs challenges, donc de manière graduée, les élèves vont apprendre à manipuler les structures algorithmiques de base et à les coder en Python.
# 
# Commands trigged by button : cmd_*
# Commands trigged by 3D scene objects : scn_*
# Commands trigged by user (student or map designer) : rp_*
# 3D scene manipulation : manip_*
###############################################################################

# Debug flag
scene = bge.logic.getCurrentScene()
scene.objects['Commands']['debug_fps']=False

# Memory
sys.setrecursionlimit(10**5)  # Limite sur la récursivité (valeur par défaut : 1000) -> segfault de Blender

# UPBGE scene
eevee = bpy.context.scene.eevee
fps_time=0.0

# Config file
rp_config = ET.parse('rp_config.xml')
rp_config_tree = rp_config.getroot()

# Sounds
audiodev = aud.Device()
snd_click = aud.Sound('asset/sounds/rp_click.ogg')
# sndbuff_click = aud.Sound.cache(snd_click)
snd_open = aud.Sound('asset/sounds/rp_open.ogg')
# sndbuff_open = aud.Sound.cache(snd_open)
snd_close = aud.Sound('asset/sounds/rp_close.ogg')
# sndbuff_close = aud.Sound.cache(snd_close)
snd_grid = aud.Sound('asset/sounds/rp_grid.ogg')
# sndbuff_grid = aud.Sound.cache(snd_grid)

# UPBGE constants
JUST_ACTIVATED = bge.logic.KX_INPUT_JUST_ACTIVATED
JUST_RELEASED = bge.logic.KX_INPUT_JUST_RELEASED
ACTIVATE = bge.logic.KX_INPUT_ACTIVE
# JUST_DEACTIVATED = bge.logic.KX_SENSOR_JUST_DEACTIVATED

###############################################################################
# User interface : texte info et compteurs
###############################################################################

color_text =  (0, 0, 0, 1) # Noir
color_text_red =  (0.799, 0.031, 0.038, 1)
color_text_orange =  (0.799, 0.176, 0.054, 1)
color_text_yellow =  (0.799, 0.617, 0.021, 1)

##
# Mise à jour de l'affichage des compteurs
##

def points_maj (cont):

    # Step
    if  scene.objects['Points-Step-text']['Text']!= str(scene.objects['Points']['step']):
        scene.objects['Points-Step-text']['Text']=str(scene.objects['Points']['step'])
        if scene.objects['Points']['upgrade_battery']:
            scene.objects['Points']['battery'] = round(100*(1 - (scene.objects['Points']['step']/200)))
        else:
            scene.objects['Points']['battery'] = round(100*(1 - (scene.objects['Points']['step']/20)))
        if scene.objects['Points']['battery'] <=10:
            scene.objects['Points-Battery'].color = color_text_red
            scene.objects['Points-Battery-text'].color = color_text_red
        if scene.objects['Points']['battery'] <=0:
            scene.objects['Points-Battery-text']['Text']="0%"
            print ("Plus de batterie !")
            scene.objects['Rover']['stop'] = True
        else:
            scene.objects['Points-Battery-text']['Text']=str(scene.objects['Points']['battery'])+"%"

    # Level
    if  scene.objects['Points-Level-text']['Text']!=str(scene.objects['Points']['level']):
        scene.objects['Points-Level-text']['Text']=str(scene.objects['Points']['level'])
        scene.objects['Book_level']['Text'] = "Niveau actuel : "+str(scene.objects['Points']['level'])

        # Maj du fichier de config (sound : data/mission/level -> [1][1].text) lors d'une augmentation de niveau
        if scene.objects['Points']['level']>int(rp_config_tree[1][1].text):
            rp_config_tree[1][1].text=str(scene.objects['Points']['level'])
            buffer_xml = ET.tostring(rp_config_tree)
            with open("rp_config.xml", "wb") as f:
                f.write(buffer_xml)

    # Nbligne
    if  scene.objects['Points-Nbligne-text']['Text']!=str(scene.objects['Points']['nbligne']):
        scene.objects['Points-Nbligne-text']['Text']=str(scene.objects['Points']['nbligne'])

    # Position du Rover 
    obj=scene.objects['Rover']
    obj['w_position']=str(round(obj.worldPosition.x,3))+","+str(round(obj.worldPosition.y,3))+","+str(round(obj.worldPosition.z,3))

###############################################################################
# Terrain
###############################################################################

##
# Initialisation lors du chargement du terrain
##

def terrain_init (cont):
    if cont.sensors['Init'].positive == False: # 1 seule fois 
        return False

    # Configuration du moteur de rendu
    eevee.use_eevee_smaa = True
   
    # Ajout du Hud
    scene.active_camera = scene.objects["Camera"]
    scene.objects['Sun'].setVisible(True,True)
    scene.addOverlayCollection(scene.cameras['Camera-Hud'], bpy.data.collections['Hud'])

    # Mémorisation des positions par défaut
    # <worldPosition.x>0.0057830810546875</worldPosition.x>
    # <worldPosition.y>-26.440298080444336</worldPosition.y>
    # <worldPosition.z>20.22315788269043</worldPosition.z>
    scene.objects['Camera']['init_lx']=0.0057830810546875
    scene.objects['Camera']['init_ly']=-26.440298080444336
    scene.objects['Camera']['init_lz']=20.22315788269043
    # scene.objects['Camera']['init_lx']=scene.objects['Camera'].worldPosition.x
    # scene.objects['Camera']['init_ly']=scene.objects['Camera'].worldPosition.y
    # scene.objects['Camera']['init_lz']=scene.objects['Camera'].worldPosition.z
    scene.objects['Camera']['past_lx']=scene.objects['Camera'].worldPosition.x
    scene.objects['Camera']['past_ly']=scene.objects['Camera'].worldPosition.y
    scene.objects['Camera']['past_lz']=scene.objects['Camera'].worldPosition.z
    scene.objects['Terrain']['init_lx']=scene.objects['Terrain'].worldPosition.x
    scene.objects['Terrain']['init_ly']=scene.objects['Terrain'].worldPosition.y
    scene.objects['Terrain']['init_lz']=scene.objects['Terrain'].worldPosition.z
    scene.objects['Terrain']['init_rx']=scene.objects['Terrain'].worldOrientation.to_euler().x
    scene.objects['Terrain']['init_ry']=scene.objects['Terrain'].worldOrientation.to_euler().y
    scene.objects['Terrain']['init_rz']=scene.objects['Terrain'].worldOrientation.to_euler().z

    # Création des balises
    scene.objects['Terrain']['map_tile_beacon']= []
    for i in range (200):
        beacon= scene.addObject("Beacon", scene.objects['Terrain'])
        beacon.worldPosition=[29,1+i,0.2]
        beacon.setVisible(False,True)
        beacon.name="Beacon-"+str(i)
        beacon.suspendPhysics (True)
        beacon['activated']=False

    # Création des lieux de forage (mission 6)
    scene.objects['Terrain']['map_tile_aim-mission6']= []
    for i in range (10):
        drill_aim= scene.addObject("Drill_aim", scene.objects['Terrain'])
        applyRotationTo(drill_aim, 40*2*math.pi*(1/360), 0, 0, False)
        drill_aim.name="Drill_aim-"+str(i)
        drill_aim.setVisible(False,True)
        drill_tile= scene.addObject("Drill_tile", scene.objects['Terrain'])
        drill_tile.name="Drill_tile-"+str(i)
        drill_tile.setVisible(False,True)

    # Init de la carte
    # Read config (mission actuelle : data/mission/current -> [1][0].text)
    scene.objects['Points']['mission']=int(rp_config_tree[1][0].text)
    rp_map.map_init()
    scene.objects['Terrain']['thread_cmd']=False
    rp_map.map_reset()

    # Récupération de la position de la caméra
    scene.objects['Camera']['current_lx'] = float(rp_config_tree[0][2][0].text)
    scene.objects['Camera']['current_ly'] = float(rp_config_tree[0][2][1].text)
    scene.objects['Camera']['current_lz'] = float(rp_config_tree[0][2][2].text)
    scene.objects['Camera'].worldPosition.x = scene.objects['Camera']['current_lx']
    scene.objects['Camera'].worldPosition.y = scene.objects['Camera']['current_ly']
    scene.objects['Camera'].worldPosition.z = scene.objects['Camera']['current_lz']

    # Camera embarquée
    # scene.objects['Camera-Rover'].setViewport(100,100,150,150)
    # scene.objects['Camera-Rover'].useViewport = True

    # width = bge.render.getWindowWidth()
    # height = bge.render.getWindowHeight()
    # cam1 = scene.objects["Camera"]
    # cam2 = scene.objects["Camera-Rover"]
    # cam1.useViewport = True
    # cam2.useViewport = True
    # cam1.setViewport(0, 0, int(width / 2), height)
    # cam2.setViewport(int(width / 2), 0, width, height)
 
##
# Mise en route et pause du cycle
##

def python_validation():
    (pylint_stdout, pylint_stderr) = lint.py_run('rp_cmd.py --disable=C --disable=W --disable=R', return_std=True)
    stdout = pylint_stdout.read()
    stderr = pylint_stderr.read()
    if " error (" in stdout: # Présence d'erreur
        print(stdout)
        return False
    else:
        return True

def terrain_run ():

    # Pause
    if scene.objects['Terrain']['run'] == True:
        scene.objects['Terrain']['run']=False
        scene.objects['Pause'].setVisible(False,False)
        scene.objects['Pause'].suspendPhysics()
        scene.objects['Pause-Hl'].setVisible(False,False)
        scene.objects['Run'].restorePhysics()
        scene.objects['Run-Hl'].setVisible(True,False)

    # Run
    else :
        scene.objects['Terrain']['run']=True
        scene.objects['Run'].setVisible(False,False)
        scene.objects['Run'].suspendPhysics()
        scene.objects['Run-Hl'].setVisible(False,False)
        # scene.objects['Pause']. restorePhysics() # FIXME pause pas implémenté
        # scene.objects['Pause'].setVisible(True,False) # FIXME pause pas implémenté

        # Démarrage de la map
        if scene.objects['Terrain']['thread_cmd']==False:
            time.sleep(0.125)
            scene.objects['Terrain']['thread_cmd']=True
            rp_map.map_reset()
            time.sleep(0.125)

            # Nombre de ligne du script Python
            # print (os.getcwd())
            file = open('rp_cmd.py', 'r')
            file_txt = file.read()
            scene.objects['Points']['nbligne'] = len(file_txt.split("\n"))-28 # 28 lignes utilisées de base
            file.close()
            scene.objects['Rover']['stop'] = False
            if python_validation():
                runpy.run_module('rp_cmd', run_name='start') # Execution du script utilisateur

        # Arrêt de la pause
        else:
            # FIXME : Relancer Ropy
            pass

##
# Arrêt et réinitialisation du cycle (forçage)
##

def terrain_stop ():
    scene.objects['Terrain']['thread_cmd']=False
    rp_map.map_reset()

##
# Fin naturelle du cycle
##

def terrain_end (cont):
    if cont.sensors['End cycle'].positive:
        scene.objects['Terrain']['run']=False
        if python_validation():
            runpy.run_module('rp_cmd', run_name='stop') # Fin du script utilisateur

        # Commandes
        scene.objects['Pause'].setVisible(False,False)
        scene.objects['Pause'].suspendPhysics()
        scene.objects['Pause-Hl'].setVisible(False,False)
        scene.objects['Run'].setVisible(True,False)
        scene.objects['Run'].restorePhysics()

##
# Vitesse du jeu
##

def terrain_speed (obj):
    speed_mode=[0.25, 0.5, 1,2,4,10]
    speed_mode_txt=["1/4", "1/2", "1", "2","4","10"]
    i=speed_mode.index(scene.objects['Commands']['speed'])

    # Affichage
    if obj.name=="Speed_up" and i<5:
        scene.objects['Commands']['speed']=speed_mode[i+1]
        scene.objects['Text_speed']['Text']=speed_mode_txt[i+1]
    if obj.name=="Speed_down" and i>0:
        scene.objects['Commands']['speed']=speed_mode[i-1]
        scene.objects['Text_speed']['Text']=speed_mode_txt[i-1]

    # Maj du fichier de config (vitesse du jeu : data/config/speed -> [0][0].text)
    rp_config_tree[0][0].text=str(scene.objects['Commands']['speed'])
    buffer_xml = ET.tostring(rp_config_tree)
    with open("rp_config.xml", "wb") as f:
        f.write(buffer_xml)

##
# Grille
##

def terrain_grid ():
    if scene.objects['Grid-u'].visible:
        scene.objects['Grid-u'].setVisible(False,True)
        scene.objects['Grid-v'].setVisible(False,True)
        rp_map.aim_hide()
    else:
        scene.objects['Grid-u']['timer'] = 0
        bpy.data.materials["Grid"].node_tree.nodes["Shader de mélange"].inputs[0].default_value = 0
        bpy.data.materials["Grid-Yellow"].node_tree.nodes["Shader de mélange"].inputs[0].default_value = 0
        bpy.data.materials["Grid-Green"].node_tree.nodes["Shader de mélange"].inputs[0].default_value = 0
        bpy.data.materials["Grid-Holo"].node_tree.nodes["Émission"].inputs[1].default_value = 0
        bpy.data.materials["Grid-Holo-Yellow"].node_tree.nodes["Émission.003"].inputs[1].default_value = 0
        bpy.data.materials["Grid-Holo-Green"].node_tree.nodes["Émission"].inputs[1].default_value = 0
        scene.objects['Grid-u'].setVisible(True,True)
        scene.objects['Grid-v'].setVisible(True,True)
        rp_map.aim_show()
        scene.objects['Grid-u']['anim'] = True

def terrain_grid_anim ():
    bpy.data.materials["Grid"].node_tree.nodes["Shader de mélange"].inputs[0].default_value=scene.objects['Grid-u']['timer']
    bpy.data.materials["Grid-Yellow"].node_tree.nodes["Shader de mélange"].inputs[0].default_value=scene.objects['Grid-u']['timer']
    bpy.data.materials["Grid-Green"].node_tree.nodes["Shader de mélange"].inputs[0].default_value=scene.objects['Grid-u']['timer']
    bpy.data.materials["Grid-Holo"].node_tree.nodes["Émission"].inputs[1].default_value =scene.objects['Grid-u']['timer']*5
    bpy.data.materials["Grid-Holo-Yellow"].node_tree.nodes["Émission.003"].inputs[1].default_value =scene.objects['Grid-u']['timer']*5
    bpy.data.materials["Grid-Holo-Green"].node_tree.nodes["Émission"].inputs[1].default_value =scene.objects['Grid-u']['timer']*5
    scene.objects['Grid-u']['timer']+=0.05
    if scene.objects['Grid-u']['timer']>= 1:
        scene.objects['Grid-u']['anim'] = False

###############################################################################
# Sons
###############################################################################

def sound_play (sound):
    if scene.objects['Commands']['sound']:
        # playsound('rp_click.ogg')
        audiodev.play(sound)
       
def sound_set ():
    scene.objects['NoSound-cmd'].suspendPhysics()
    scene.objects['NoSound-cmd'].setVisible(False,False)
    scene.objects['NoSound-cmd-Hl'].setVisible(False,False)
    scene.objects['Sound-cmd'].restorePhysics()
    scene.objects['Sound-cmd-Hl'].setVisible(True,False)
    scene.objects['Commands']['sound']=True
    # scene.objects['Cmd-text']['Text']= "Mute"
    scene.objects['Cmd-text']['Text']= "Muet"
    # scene.objects['Cmd-text'].setVisible(True,False)

    # Maj du fichier de config (sound : data/config/sound -> [0][1].text)
    rp_config_tree[0][1].text=str(scene.objects['Commands']['sound'])
    buffer_xml = ET.tostring(rp_config_tree)
    with open("rp_config.xml", "wb") as f:
        f.write(buffer_xml)

def sound_unset ():
    scene.objects['Sound-cmd'].suspendPhysics()
    scene.objects['Sound-cmd'].setVisible(False,False)
    scene.objects['Sound-cmd-Hl'].setVisible(False,False)
    scene.objects['NoSound-cmd'].restorePhysics()
    scene.objects['NoSound-cmd-Hl'].setVisible(True,False)
    scene.objects['Commands']['sound']=False
    # scene.objects['Cmd-text']['Text']= "Unmute"
    scene.objects['Cmd-text']['Text']= "Rétablir le son"
    # scene.objects['Cmd-text'].setVisible(True,False)

    # Maj du fichier de config (sound : data/config/sound -> [0][1].text)
    rp_config_tree[0][1].text=str(scene.objects['Commands']['sound'])
    buffer_xml = ET.tostring(rp_config_tree)
    with open("rp_config.xml", "wb") as f:
        f.write(buffer_xml)

###############################################################################
# Commandes
###############################################################################

color_cmd =  (0.8, 0.8, 0.8, 1) # Blanc
color_cmd_hl =  (0.8, 0.619, 0.021, 1) # Jaune

##
# Init
##

def cmd_init(cont):
    if cont.sensors['Init'].positive == False: # 1 seule fois 
        return False

    # Taille de la fenêtre
    # Read config (screen size : data/config/screen/width-> [0][3][0].text)
    bge.render.setWindowSize(int(rp_config_tree[0][3][0].text),int(rp_config_tree[0][3][1].text))
    
   # UI : Commands
    scene.objects['Run-Hl'].setVisible(False,False)
    scene.objects['Pause'].setVisible(False,False)
    scene.objects['Pause'].suspendPhysics()
    scene.objects['Pause-Hl'].setVisible(False,False)
    scene.objects['Stop-Hl'].setVisible(False,False)
    scene.objects['Aim-cmd-Hl'].setVisible(False,False)
    scene.objects['Doc-cmd-Hl'].setVisible(False,False)
    scene.objects['Task-cmd-Hl'].setVisible(False,False)
    scene.objects['Task_close-cmd'].setVisible(False,False)
    scene.objects['Task_close-cmd'].suspendPhysics()
    scene.objects['Task_close-cmd-Hl'].setVisible(False,False)
    scene.objects['Store-cmd-Hl'].setVisible(False,False)
    scene.objects['ResetView-Hl'].setVisible(False,False)
    scene.objects['About-cmd-Hl'].setVisible(False,False)
    scene.objects['Speed_up-Hl'].setVisible(False,False)
    scene.objects['Speed_down-Hl'].setVisible(False,False)

    # UI : Sounds
    # Read config (sound : data/config/sound -> [0][1].text)
    if rp_config_tree[0][1].text == "True":
        sound_set ()
    else:
        sound_unset ()
    # audiodev.unlock()

    # UI : Text, ...
    scene.objects['Cmd-text']['Text']=""
    scene.objects['Cmd-text'].setVisible(True,False)
    scene.objects['Points-Map-text']['Text']=""
    scene.objects['Grid-u'].setVisible(False,True)
    scene.objects['Grid-v'].setVisible(False,True)
    scene.objects['Points-Map-text']['Text']=""
    scene.objects['Points-Map-text'].setVisible(True,False)
    scene.objects['Points-Twins'].setVisible(False,True)
    scene.objects['Points-Twins-text'].setVisible(False,False)
    # scene.objects['Info-1-text'].setVisible(False,False)
    # scene.objects['Info-2-text'].setVisible(False,False)

    # UI : Mouse
    # Window size : 738.5 415.5
    if scene.objects['Mouse_main']['mouse_graphic']:
        bge.render.setMousePosition(int(bge.render.getWindowWidth() / 2), int(bge.render.getWindowHeight() / 2))
        bge.render.showMouse(scene.objects['Commands']['debug_mouse'])
        scene.objects['Mouse_main'].worldPosition = [0.07, -8.11, 4.71035] # Vielle version : [0.118161, -8.24305, 4.71035] ; [0, -3.5, 2], [0, -8, 6]
        scene.objects['Mouse_main'].worldScale=[30, 30, 30]
        scene.objects['Mouse_main']['past_x']=0
        scene.objects['Mouse_main']['past_y']=0
        scene.objects['Mouse_main']['mouse_up']=0
    else:
        scene.objects['Mouse_main'].setVisible(False,False)
        bge.render.showMouse(True)

    # Speed
    # Read config (game speed : data/config/speed -> [0][0].text)
    speed_mode=[0.25, 0.5, 1,2,4,10]
    speed_mode_txt=["1/4", "1/2", "1", "2","4","10"]
    if scene.objects['Points']['upgrade_speed'] :
        scene.objects['Commands']['speed']=float(rp_config_tree[0][0].text)
    else:
        scene.objects['Commands']['speed']=1.00
    i=speed_mode.index(scene.objects['Commands']['speed'])
    scene.objects['Text_speed']['Text']=speed_mode_txt[i]
    
    # Missions
    # Read config (mission actuelle : data/mission/current -> [1][0].text)
    # Read config (niveau atteint : data/mission/level -> [1][1].text)
    scene.objects['Points']['mission']=int(rp_config_tree[1][0].text)
    scene.objects['Points']['mission_init']= scene.objects['Points']['mission']
    scene.objects['Points']['level']=int(rp_config_tree[1][1].text)
    scene.objects['Points-Map-text']['Text']="Mission "+str(scene.objects['Points']['mission'])
    scene.objects['Book_mission']['Text'] = "Mission en cours : "+str(scene.objects['Points']['mission'])
    scene.objects['Book_level']['Text'] = "Niveau actuel : "+str(scene.objects['Points']['level'])

    # Upgrade
    upgrade_maj()
    
    # Windows
    windows=("Doc", "Doc_chap-general", "Doc_chap-missions", "Doc_chap-rover", "Doc_chap-python", "About", "Task")
    for window in windows:
        scene.objects[window].setVisible(False,True)
    rp_doc.init()
    rp_store.init()
    scene.objects['Task'].worldPosition = [42.6047, -2.09252, 2.99685] # Panel task

##
# Highlight des commandes
##

def cmd_hl(cont):
    obj = cont.owner

    # Activation
    if cont.sensors['MO'].status == JUST_ACTIVATED and scene.objects['Terrain']['manip_mode']==0:
        if obj.name!="Run" and obj.name!="Pause" and obj.name!="Stop" and obj.name!="Doc-cmd-colbox":
            obj.setVisible(False,True)
            scene.objects[obj.name+'-Hl'].setVisible(True,True)

        # Run et pause
        if obj.name=="Pause" or obj.name=="Run":
            if scene.objects['Terrain']['run'] == True:
                pass
                # scene.objects['Pause'].setVisible(False,False) FIXME pause pas implémenté
                # scene.objects['Pause-Hl'].setVisible(True,False) FIXME pause pas implémenté
            else:
                scene.objects['Run'].setVisible(False,False)
                scene.objects['Run-Hl'].setVisible(True,False)

        # Stop
        if obj.name=="Stop":
            scene.objects['Stop'].setVisible(False,False)
            scene.objects['Stop-Hl'].setVisible(True,False)

        # Doc
        if obj.name=="Doc-cmd-colbox":
            scene.objects['Doc-cmd'].setVisible(False,False)
            scene.objects['Doc-cmd-Hl'].setVisible(True,False)

        # Text
        text_hl ={"Run":"Exécuter (F5)",
                  "Stop":"Stop et initialisation (F6)",
                  "Pause":"Pause (F5)",
                  "Aim-cmd":"Afficher/cacher l'objectif",
                  "Doc-cmd-colbox":"Documentation",
                  "Store-cmd":"Boutique",
                  "Task-cmd":"Liste des tâches",
                  "Task_close-cmd":"Fermer la liste des tâches",
                  "ResetView":  "Reset de la vue (Touche Début)",
                  "About-cmd":  "A propos",
                  "Speed_down":  "Moins vite (-)",
                  "Speed_up":  "Plus vite (+)",
                  "Sound-cmd":  "Muet",
                  "NoSound-cmd":  "Rétablir le son"}
        # text_hl ={"Run":"Run (F5)",
        #           "Stop":"Stop (F6)",
        #           "Pause":"Pause (F5)",
        #           "Aim":"Show aim",
        #           "Doc-cmd":"Documentation",
        #           "ResetView":  "Reset view (Home key)",
        #           "About-cmd":  "About",
        #           "Speed_down":  "Speed down (-)",
        #           "Speed_up":  "Speed up (+)",
        #           "Sound-cmd":  "Mute",
        #           "NoSound-cmd":  "Unmute"}
        scene.objects['Cmd-text']['Text']= text_hl[obj.name]
        scene.objects['Cmd-text'].setVisible(True,False)

    # Désactivation
    if cont.sensors['MO'].status == JUST_RELEASED and (scene.objects['Terrain']['manip_mode']==0 or scene.objects['Terrain']['manip_mode']==9):
        scene.objects['Cmd-text']['Text']= ""
        scene.objects['Cmd-text'].setVisible(False,False)
        if obj.name!="Run" and obj.name!="Pause" and obj.name!="Stop" and obj.name!="Sound-cmd" and obj.name!="NoSound-cmd"  and obj.name!="Task-cmd" and obj.name!="Task_close-cmd"  and obj.name!="Doc-cmd-colbox":
            scene.objects[obj.name+'-Hl'].setVisible(False,True)
            obj.setVisible(True,True)

        # Run et pause
        if obj.name=="Pause" or obj.name=="Run":
            if scene.objects['Terrain']['run'] == True:
                pass
                # scene.objects['Pause-Hl'].setVisible(False,False) FIXME pause pas implémenté
                # scene.objects['Pause'].setVisible(True,False) FIXME pause pas implémenté
            else:
                scene.objects['Run-Hl'].setVisible(False,False)
                scene.objects['Run'].setVisible(True,False)

        # Stop
        if obj.name=="Stop":
            scene.objects['Stop-Hl'].setVisible(False,False)
            scene.objects['Stop'].setVisible(True,False)

        # Doc
        if obj.name=="Doc-cmd-colbox":
            scene.objects['Doc-cmd-Hl'].setVisible(False,False)
            scene.objects['Doc-cmd'].setVisible(True,False)

        # Sound
        if obj.name=="NoSound-cmd" and scene.objects['Commands']['sound']==False:
            scene.objects['NoSound-cmd-Hl'].setVisible(False,False)
            scene.objects['NoSound-cmd'].setVisible(True,False)
        if obj.name=="Sound-cmd" and scene.objects['Commands']['sound']==True:
            scene.objects['Sound-cmd-Hl'].setVisible(False,False)
            scene.objects['Sound-cmd'].setVisible(True,False)

        # Task panel
        if obj.name=="Task-cmd":
            scene.objects['Task-cmd-Hl'].setVisible(False,True)
            if scene.objects['Commands']['task']:
                scene.objects['Task-cmd'].setVisible(False,True)
            else:
                scene.objects['Task-cmd'].setVisible(True,False)
        if obj.name=="Task_close-cmd":
            scene.objects['Task_close-cmd-Hl'].setVisible(False,True)
            if scene.objects['Commands']['task']==False:
                scene.objects['Task_close-cmd'].setVisible(False,True)
            else:
                scene.objects['Task_close-cmd'].setVisible(True,False)

##
# Click sur les commandes
##

def cmd_click (cont):
    obj = cont.owner
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive and scene.objects['Terrain']['manip_mode']==0:

        if obj.name=="Pause" or obj.name=="Run":
            sound_play (snd_click)
            terrain_run ()
            
        if obj.name=="Stop":
            sound_play (snd_click)
            terrain_stop ()
            
        if obj.name=="Aim-cmd":
            sound_play (snd_grid)
            terrain_grid ()
            
        if obj.name=="Speed_up" or obj.name=="Speed_down":
            sound_play (snd_click)
            terrain_speed (obj)
            
        if obj.name=="ResetView":
            sound_play (snd_click)
            manip_reset()
        if obj.name=="Sound-cmd":
            sound_unset ()
        if obj.name=="NoSound-cmd":
            sound_set ()
            sound_play (snd_click)

        if obj.name=="Doc-cmd-colbox":
            sound_play (snd_open)
            tablet_open ()
        if obj.name=="About-cmd":
            sound_play (snd_open)
            about_open ()
        if obj.name=="Store-cmd":
            sound_play (snd_open)
            store_open ()
        if obj.name=="Task-cmd":
            sound_play (snd_open)
            task_open ()
        if obj.name=="Task_close-cmd":
            task_close ()

###############################################################################
# Gestion du clavier
###############################################################################

##
# Mode
#
#  0 : rien (par défaut)
# 1 : Pan avec Shift
# 2 : Zoom avec Ctrl
# 8 : Fenêtre Documentation
# 9 : Fenêtre About
##

def  mode(cont):
    obj = cont.owner
    keyboard = bge.logic.keyboard
   
    # Touche ESC
    if JUST_ACTIVATED in keyboard.inputs[bge.events.ESCKEY].queue:

        # Fenêtres modales
        if scene.objects['Terrain']['manip_mode']==9: # About
            if scene.objects['About'].visible:
                about_close()
                return
        elif scene.objects['Terrain']['manip_mode']==8: # Doc et store
            if scene.objects['Doc'].visible:
                tablet_close()
                return
            if scene.objects['Store'].visible:
                store_close()
                return
        else: # Sortir du jeu
            terrain_stop ()

            # Maj du fichier de config (position de la camera : data/config/cam_x,y,z -> [0][2][0].text, [0][2][1].text, [0][2][2].text)
            rp_config_tree[0][2][0].text=str(scene.objects['Camera']['current_lx'])
            rp_config_tree[0][2][1].text=str(scene.objects['Camera']['current_ly'])
            rp_config_tree[0][2][2].text=str(scene.objects['Camera']['current_lz'])
            # print ("Fin :", scene.objects['Camera'].worldPosition.x, scene.objects['Camera'].worldPosition.y, scene.objects['Camera'].worldPosition.z)
            buffer_xml = ET.tostring(rp_config_tree)
            with open("rp_config.xml", "wb") as f:
                f.write(buffer_xml)
            bge.logic.endGame()

    # Fenêtre modale (inhibition des touches hors ESC)
    if scene.objects['Terrain']['manip_mode']==9:
        return
   
    # Shift -> mode 1 : Pan (clic milieu)
    if JUST_ACTIVATED in keyboard.inputs[bge.events.LEFTSHIFTKEY].queue:
        obj['manip_mode']=1
    if JUST_ACTIVATED in keyboard.inputs[bge.events.RIGHTSHIFTKEY].queue:
        obj['manip_mode']=1

    # Ctrl -> mode 2 : Zoom (clic milieu)
    if JUST_ACTIVATED in keyboard.inputs[bge.events.LEFTCTRLKEY].queue:
        obj['manip_mode']=2
    if JUST_ACTIVATED in keyboard.inputs[bge.events.RIGHTCTRLKEY].queue:
        obj['manip_mode']=2

    # Pas de modificateur -> mode 0 : Pas de Orbit (mode 0) ici
    if  JUST_RELEASED in keyboard.inputs[bge.events.LEFTSHIFTKEY].queue:
        obj['manip_mode']=0
    if  JUST_RELEASED in keyboard.inputs[bge.events.RIGHTSHIFTKEY].queue:
        obj['manip_mode']=0
    if  JUST_RELEASED in keyboard.inputs[bge.events.LEFTCTRLKEY].queue:
         obj['manip_mode']=0
    if  JUST_RELEASED in keyboard.inputs[bge.events.RIGHTCTRLKEY].queue:
         obj['manip_mode']=0

    # Touche Home -> Reset de la vue
    if JUST_ACTIVATED in keyboard.inputs[bge.events.HOMEKEY].queue:
        manip_reset()
        # if scene.objects['Mouse_main']['mouse_graphic']:
        #     bge.render.setMousePosition(int(bge.render.getWindowWidth() / 2), int(bge.render.getWindowHeight() / 2))
        #     scene.objects['Mouse_main'].worldPosition = [0.07, -8.11, 4.71035] # Vielle version : [0.118161, -8.24305, 4.71035] ; [0, -3.5, 2], [0, -8, 6]
        #     scene.objects['Mouse_main'].worldScale=[30, 30, 30]
        #     scene.objects['Mouse_main']['past_x']=0
        #     scene.objects['Mouse_main']['past_y']=0

    # Touche F5 -> Run et Pause
    if JUST_ACTIVATED in keyboard.inputs[bge.events.F5KEY].queue:
        terrain_run ()

    # Touche F6 -> Stop / Init
    if JUST_ACTIVATED in keyboard.inputs[bge.events.F6KEY].queue:
        terrain_stop ()
        # if scene.objects['Terrain']['thread_cmd']==True: # FIXME : bien utile ?
        #     terrain_stop ()

    # Touche +/- du pad -> Vitesse + ou /
    if JUST_ACTIVATED in keyboard.inputs[bge.events.PADPLUSKEY].queue:
        if scene.objects['Points']['upgrade_speed'] :
            terrain_speed (scene.objects['Speed_up'])
    if JUST_ACTIVATED in keyboard.inputs[bge.events.PADMINUS].queue:
        if scene.objects['Points']['upgrade_speed'] :
            terrain_speed (scene.objects['Speed_down'])

###############################################################################
# Manipulation 3D de la scène
###############################################################################

##
# Atteindre une orientation  (bas niveau)
##

def applyRotationTo(obj, rx=None, ry=None, rz=None, Local=True):
    rres=0.001 # resolution rotation

    # x
    if rx is not None:
        while (abs(rx-obj.worldOrientation.to_euler().x) > rres) :
            if obj.worldOrientation.to_euler().x-rx > rres:
                obj.applyRotation((-rres, 0, 0), Local)
            if rx-obj.worldOrientation.to_euler().x > rres:
                obj.applyRotation((rres, 0, 0), Local)
            # print ("delta x ",rx-obj.worldOrientation.to_euler().x)
       
    # y
    if ry is not None:
        while (abs(ry-obj.worldOrientation.to_euler().y) > rres) :
            if obj.worldOrientation.to_euler().y-ry > rres:
                obj.applyRotation((0, -rres, 0), Local)
            if ry-obj.worldOrientation.to_euler().y > rres:
                obj.applyRotation((0, rres, 0), Local)
            # print ("delta y ",ry-obj.worldOrientation.to_euler().y)

    # z
    if rz is not None:
        while (abs(rz-obj.worldOrientation.to_euler().z) > rres) :
            if obj.worldOrientation.to_euler().z-rz > rres:
                obj.applyRotation((0, 0, -rres), Local)
            if rz-obj.worldOrientation.to_euler().z > rres:
                obj.applyRotation((0, 0, rres), Local)
            # print ("delta z ",rz-obj.worldOrientation.to_euler().z)

##
# Reset de la manipulation de la vue
##

def  manip_reset():
    scene.objects['Camera'].worldPosition.x = scene.objects['Camera']['init_lx']
    scene.objects['Camera'].worldPosition.y = scene.objects['Camera']['init_ly']
    scene.objects['Camera'].worldPosition.z = scene.objects['Camera']['init_lz']
    scene.objects['Camera']['current_lx'] = scene.objects['Camera'].worldPosition.x
    scene.objects['Camera']['current_ly'] = scene.objects['Camera'].worldPosition.y
    scene.objects['Camera']['current_lz'] = scene.objects['Camera'].worldPosition.z
    applyRotationTo(scene.objects['Terrain'], 0, 0, 0)
    scene.objects['Cmd-text']['Text']= ""

##
# Position de  départ pour la manipulation de la vue
##

def  manip_start(cont):
    if scene.objects['Terrain']['manip_mode']!=9:
        obj = cont.owner
        obj['click_x']=cont.sensors['ClickM'].position[0]
        obj['click_y']=cont.sensors['ClickM'].position[1]

##
# Cacher le cercle de la manipulation Orbit
##

def  manip_stop(cont):
    scene.objects['Camera']['current_lx'] = scene.objects['Camera'].worldPosition.x
    scene.objects['Camera']['current_ly'] = scene.objects['Camera'].worldPosition.y
    scene.objects['Camera']['current_lz'] = scene.objects['Camera'].worldPosition.z
    # scene.objects['Orbit'].setVisible(False,False)


##
# Manipulation du modèle ou de la caméra
##

def  manip(cont):
    obj = cont.owner
    sensibilite_orbit=0.00005 # Base : 0.0005
    sensibilite_pan=0.001 # Base : 0.005
    sensibilite_zoom=0.005 # Base : 0.01
    delta_x=cont.sensors['DownM'].position[0]-obj['click_x']
    delta_y=cont.sensors['DownM'].position[1]-obj['click_y']

    # Pan
    if obj['manip_mode']==1: # Shift
        scene.objects['Camera'].applyMovement((delta_x*-sensibilite_pan, delta_y*sensibilite_pan, 0), True)
        # scene.objects['Commands-colbox'].applyMovement((delta_x*-sensibilite_pan, delta_y*sensibilite_pan, 0), True)
        # scene.objects['Commands-colbox'].applyMovement((delta_x*-sensibilite_pan, delta_y*sensibilite_pan*math.cos(50*2*math.pi*(1/360)), delta_y*sensibilite_pan*math.sin(50*2*math.pi*(1/360))), True)

        # if scene.objects['Mouse_main']['mouse_graphic']:
        #     scene.objects['Mouse_main'].applyMovement((delta_x*-sensibilite_pan, delta_y*sensibilite_pan, 0), True)

    # Zoom FIXME : marche pas au niveau de la souris
    if obj['manip_mode']==2: # Ctrl
        # if scene.objects['Mouse_main']['mouse_graphic']:
        #     position_scale_x = 0.0005
        #     position_scale_y = position_scale_x
        #     mouse_x=scene.objects['Mouse_main'].sensors["Mouse"].position[0]-int(bge.render.getWindowWidth() / 2)
        #     mouse_y=scene.objects['Mouse_main'].sensors["Mouse"].position[1]-int(bge.render.getWindowHeight() / 2)
        #     distance_cam_past= math.sqrt(scene.objects['Camera']['past_ly']**2+scene.objects['Camera']['past_lz']**2)
        #     distance_cam = math.sqrt((scene.objects['Camera'].worldPosition.y**2+scene.objects['Camera'].worldPosition.z**2))
        #     size_scale = (distance_cam/distance_cam_past) * 0.23
        if scene.objects['Camera'].worldPosition.z >0.5 and scene.objects['Camera'].worldPosition.z <57:
            scene.objects['Camera'].applyMovement((0, 0, (delta_x+delta_y)*sensibilite_zoom), True)
        if scene.objects['Camera'].worldPosition.z <=0.5 or scene.objects['Camera'].worldPosition.z >=57 :
            scene.objects['Camera'].applyMovement((0, 0, -(delta_x+delta_y)*sensibilite_zoom), True)
        # if scene.objects['Mouse_main']['mouse_graphic']:
        #     scene.objects['Mouse_main'].applyMovement((mouse_x*(delta_x+delta_y)*sensibilite_zoom*position_scale_x, -mouse_y*(delta_x+delta_y)*sensibilite_zoom*position_scale_y, 0), True)
        #     scene.objects['Mouse_main'].worldScale *= (delta_x+delta_y)*sensibilite_zoom*size_scale
        # scene.objects['Camera']['past_ly']=scene.objects['Camera'].worldPosition.y
        # scene.objects['Camera']['past_lz']=scene.objects['Camera'].worldPosition.z

##
# Manipulation du modèle ou de la caméra
##

def  manip_wheel(cont):
    if scene.objects['Terrain']['manip_mode']!=9: # Fenêtre modale
        obj = cont.owner
        # sensibilite_wheel = 5 # Base : 20
        sensibilite_wheel = scene.objects['Camera'].worldPosition.z/5 # Sensibilité progressive
        # if scene.objects['Mouse_main']['mouse_graphic']:
        #     position_scale_x = 0.0005
        #     position_scale_y = position_scale_x
        #     mouse_x=scene.objects['Mouse_main'].sensors["Mouse"].position[0]-int(bge.render.getWindowWidth() / 2)
        #     mouse_y=scene.objects['Mouse_main'].sensors["Mouse"].position[1]-int(bge.render.getWindowHeight() / 2)
        #     distance_cam_past = math.sqrt((scene.objects['Camera'].worldPosition.y**2+scene.objects['Camera'].worldPosition.z**2))
        #     # size_scale = (distance_cam/distance_cam_past) * 0.23
        #     size_scale = 0.2
        #     # size_scale = 0.23
        if cont.sensors['WheelUp'].positive :
            scene.objects['Camera'].applyMovement((0, 0, -sensibilite_wheel), True)
            # if scene.objects['Mouse_main']['mouse_graphic']:
            #     distance_cam = math.sqrt((scene.objects['Camera'].worldPosition.y**2+scene.objects['Camera'].worldPosition.z**2))
            #     scene.objects['Mouse_main'].applyMovement((-mouse_x*sensibilite_wheel*position_scale_x, mouse_y*sensibilite_wheel*position_scale_y, 0), True)
            #     scene.objects['Mouse_main'].worldScale *= sensibilite_wheel* (distance_cam/distance_cam_past) *size_scale
        elif cont.sensors['WheelDown'].positive and scene.objects['Camera'].worldPosition.z<50:
            scene.objects['Camera'].applyMovement((0, 0, sensibilite_wheel), True)
            # if scene.objects['Mouse_main']['mouse_graphic']:
            #     distance_cam = math.sqrt((scene.objects['Camera'].worldPosition.y**2+scene.objects['Camera'].worldPosition.z**2))
            #     scene.objects['Mouse_main'].applyMovement((mouse_x*sensibilite_wheel*position_scale_x, -mouse_y*sensibilite_wheel*position_scale_y, 0), True)
            #     scene.objects['Mouse_main'].worldScale /= sensibilite_wheel* (distance_cam_past/distance_cam) *size_scale
        else:
            return
        manip_stop(cont)

##
# Icone de la souris
##

def  mouse(cont):
    if scene.objects['Mouse_main']['mouse_graphic']==False:
        return
    obj = cont.owner

    # Ancienne version basée sur la position de la caméra
    # distance_cam_init= math.sqrt(scene.objects['Camera']['init_ly']**2+scene.objects['Camera']['init_lz']**2)
    # distance_cam = math.sqrt((scene.objects['Camera'].worldPosition.y**2+scene.objects['Camera'].worldPosition.z**2))
    # ratio = ((distance_cam_init - distance_cam)/distance_cam_init)*1.39 # trop vite -> +, pas assez vite -> -
    # scale_x=0.0118-0.0118*ratio
    # scale_y=scale_x
    # delta_x=cont.sensors["Mouse"].position[0]-obj['past_x']
    # delta_y=cont.sensors["Mouse"].position[1]-obj['past_y']
    # if delta_x<500 and delta_y<500:
    #     scene.objects['Mouse_main'].worldPosition.x += delta_x*scale_x
    #     scene.objects['Mouse_main'].worldPosition.y -= delta_y*scale_y*math.cos(50*2*math.pi*(1/360))
    #     scene.objects['Mouse_main'].worldPosition.z -= delta_y*scale_y*math.sin(50*2*math.pi*(1/360))
    # scene.objects['Mouse_main']['past_x']=cont.sensors["Mouse"].position[0]
    # scene.objects['Mouse_main']['past_y']=cont.sensors["Mouse"].position[1]

    # Version basée sur obj.getDistanceTo(scene.objects['Camera'])
    delta_x=cont.sensors["Mouse"].position[0]-obj['past_x']
    delta_y=cont.sensors["Mouse"].position[1]-obj['past_y']
    vect=mathutils.Vector((1,1,1))-obj.getVectTo(scene.objects['Camera'])[1]
    dist= obj.getDistanceTo(scene.objects['Camera'])
    # print ("delta_x, delta_y, vect, dist : ", delta_x, delta_y, vect, dist)
    if obj['past_dist']==0:
        obj['past_dist']=obj.getDistanceTo(scene.objects['Camera'])
    ratio = dist/obj['past_dist']
    # print ("delta_x, delta_y, vect, dist : ", delta_x, delta_y, vect, dist)
    scale_x=ratio*0.016
    scale_y=ratio*0.0162
    # scale_xy=ratio*0.0005
    scale_xy=ratio*0
    if delta_x<500 and delta_y<500:
        # scene.objects['Mouse_main'].applyMovement((delta_x*scale_x, -vect[1]*delta_y*scale_y, -vect[1]*delta_y*scale_y), False)
        scene.objects['Mouse_main'].applyMovement((delta_x*scale_x+delta_x*delta_y*scale_xy,
                                                   -delta_y*scale_y*math.cos(50*2*math.pi*(1/360)),
                                                   -delta_y*scale_y*math.sin(50*2*math.pi*(1/360))), False)
    obj['past_x']=cont.sensors["Mouse"].position[0]
    obj['past_y']=cont.sensors["Mouse"].position[1]

##
# Mise en avant de la souris
##

def mouse_up():
    scene.objects['Mouse_main']['mouse_up']+=1
    if scene.objects['Mouse_main']['mouse_up'] == 1:
        decal = 18
        size_scale = 0.2
        # print (scene.objects['Mouse_main'].getVectTo(scene.objects['Camera'])[1])
        vect=scene.objects['Mouse_main'].getVectTo(scene.objects['Camera'])[1]
        dist_past= scene.objects['Mouse_main'].getDistanceTo(scene.objects['Camera'])
        scene.objects['Mouse_main'].applyMovement((vect[0]*decal, vect[1]*decal, vect[2]*decal), False)
        dist= scene.objects['Mouse_main'].getDistanceTo(scene.objects['Camera'])
        scene.objects['Mouse_main'].worldScale *= (dist/dist_past) *size_scale
        scene.objects['Mouse_main'].worldScale=[8, 8, 8]

##
# Mise en arrière de la souris
##

def mouse_down():
    scene.objects['Mouse_main']['mouse_up']-=1
    if scene.objects['Mouse_main']['mouse_up'] == 0:
        decal = 18
        size_scale = 0.2
        # print (scene.objects['Mouse_main'].getVectTo(scene.objects['Camera'])[1])
        vect=scene.objects['Mouse_main'].getVectTo(scene.objects['Camera'])[1]
        dist_past= scene.objects['Mouse_main'].getDistanceTo(scene.objects['Camera'])
        scene.objects['Mouse_main'].applyMovement((-vect[0]*decal, -vect[1]*decal, -vect[2]*decal), False)
        dist= scene.objects['Mouse_main'].getDistanceTo(scene.objects['Camera'])
        scene.objects['Mouse_main'].worldScale /= (dist_past/dist) *size_scale
        scene.objects['Mouse_main'].worldScale=[30, 30, 30]

###############################################################################
# Documentation
###############################################################################

##
# Allumer la tablette
##

def  tablet_open ():

    # Fenêtre
    scene.objects['Terrain']['manip_mode']=8 # Fenêtre modale Aide
    scene.objects['Camera']['current_lx'] = scene.objects['Camera'].worldPosition.x
    scene.objects['Camera']['current_ly'] = scene.objects['Camera'].worldPosition.y
    scene.objects['Camera']['current_lz'] = scene.objects['Camera'].worldPosition.z
    scene.objects['Camera'].worldPosition.x = scene.objects['Camera']['init_lx']
    scene.objects['Camera'].worldPosition.y = scene.objects['Camera']['init_ly']
    scene.objects['Camera'].worldPosition.z = scene.objects['Camera']['init_lz']
    applyRotationTo(scene.objects['Terrain'], 0, 0, 0)
    scene.objects['Cmd-text']['Text']= ""

    # Overlay
    scene.removeOverlayCollection(bpy.data.collections['Hud'])
    scene.objects['Points'].setVisible(False,True)
    scene.objects['Commands'].setVisible(False,True)
    scene.active_camera = scene.objects["Camera-Hud"]
    scene.objects['Camera'].setVisible(False,True)

    scene.objects['Doc-cmd-colbox'].suspendPhysics (True)

    # Ouvrir la tablette
    rp_doc.open()

##
# Fermeture de la tablette
##

def  tablet_close ():
    rp_doc.close()
    scene.objects['Terrain']['manip_mode']=0 # Enlever la fenêtre modale

    # Maj du fichier de config (mission actuelle : data/mission/current -> [1][0].text)
    rp_config_tree[1][0].text=str(scene.objects['Points']['mission'])
    buffer_xml = ET.tostring(rp_config_tree)
    with open("rp_config.xml", "wb") as f:
        f.write(buffer_xml)

    # Overlay
    scene.objects['Points'].setVisible(True,True)
    if scene.objects['Commands']['twins']:
        scene.objects['Points-Twins'].setVisible(True,True)
        scene.objects['Points-Twins-text'].setVisible(True,False)
    else:
        scene.objects['Points-Twins'].setVisible(False,True)
        scene.objects['Points-Twins-text'].setVisible(False,False)
    scene.objects['Commands'].setVisible(True,True)
    scene.objects['Camera'].setVisible(True,True)
    scene.active_camera = scene.objects["Camera"]
    scene.addOverlayCollection(scene.cameras['Camera-Hud'], bpy.data.collections['Hud'])

    # UI : Commands
    scene.objects['Run-Hl'].setVisible(False,False)
    scene.objects['Pause'].setVisible(False,False)
    scene.objects['Pause'].suspendPhysics()
    scene.objects['Pause-Hl'].setVisible(False,False)
    # scene.objects['Stop'].setVisible(False,False)
    # scene.objects['Stop'].suspendPhysics()
    scene.objects['Stop-Hl'].setVisible(False,False)
    scene.objects['Aim-cmd-Hl'].setVisible(False,False)
    scene.objects['Doc-cmd-Hl'].setVisible(False,False)
    scene.objects['Doc-cmd-colbox'].restorePhysics()
    scene.objects['ResetView-Hl'].setVisible(False,False)
    scene.objects['About-cmd-Hl'].setVisible(False,False)
    scene.objects['Speed_up-Hl'].setVisible(False,False)
    scene.objects['Speed_down-Hl'].setVisible(False,False)
    scene.objects['Cmd-text'].setVisible(False,False)

    # Task panel
    if scene.objects['Commands']['task']:
        scene.objects['Task-cmd'].setVisible(False,True)
        scene.objects['Task-cmd'].suspendPhysics()
        scene.objects['Task-cmd-Hl'].setVisible(False,True)
        scene.objects['Task_close-cmd'].setVisible(True,True)
        scene.objects['Task_close-cmd'].restorePhysics()
        scene.objects['Task_close-cmd-Hl'].setVisible(False,True)
    else:
        scene.objects['Task_close-cmd'].setVisible(False,True)
        scene.objects['Task_close-cmd'].suspendPhysics()
        scene.objects['Task_close-cmd-Hl'].setVisible(False,True)
        scene.objects['Task-cmd'].setVisible(True,True)
        scene.objects['Task-cmd'].restorePhysics()
        scene.objects['Task-cmd-Hl'].setVisible(False,True)

    # Vitesse
    if scene.objects['Points']['upgrade_speed'] ==False:
        scene.objects['Text_speed'].setVisible(False,True)
        scene.objects['Text_speed_label-fr'].setVisible(False,True)
        scene.objects['Speed_down'].setVisible(False,True)
        scene.objects['Speed_up'].setVisible(False,True)

    # Camera
    scene.objects['Camera'].worldPosition.x = scene.objects['Camera']['current_lx']
    scene.objects['Camera'].worldPosition.y = scene.objects['Camera']['current_ly']
    scene.objects['Camera'].worldPosition.z = scene.objects['Camera']['current_lz']

    # Reset si changement de mission
    if scene.objects['Points']['mission_init']!= scene.objects['Points']['mission']:
        terrain_stop ()
        rp_map.task()
        if scene.objects['Grid-u'].visible:
            rp_map.aim_show()
        scene.objects['Points']['mission_init'] = scene.objects['Points']['mission']

##
# Clic pour fermer la tablette
##

def tablet_close_click(cont):
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive :
        sound_play (snd_close)
        tablet_close()

###############################################################################
# Liste des tâches
###############################################################################

##
# Allumer la tablette des tâches
##

def  task_open ():
    scene.objects['Commands']['task']=True
    scene.objects['Task-cmd'].setVisible(False,True)
    scene.objects['Task-cmd'].suspendPhysics()
    scene.objects['Task-cmd-Hl'].setVisible(False,True)
    scene.objects['Task'].setVisible(True,True)
    scene.objects['Task']['timer'] = 0
    scene.objects['Task']['anim_open'] = True
    rp_map.task()

def task_open_anim():
    x0_cam=0.005783
    y0_cam=-26.4403
    z0_cam=20.2232
    x_cam=scene.objects['Camera'].worldPosition.x
    y_cam=scene.objects['Camera'].worldPosition.y
    z_cam=scene.objects['Camera'].worldPosition.z
    resol=50
    x0=0.291678-x0_cam+x_cam
    y0 =-25.9416-y0_cam+y_cam
    z0 = 19.793-z0_cam+z_cam
    x1 = 0.201271-x0_cam+x_cam
    y1 =-25.9416-y0_cam+y_cam
    z1 = 19.793-z0_cam+z_cam
    xi = x0+((x1-x0)/resol)*scene.objects['Task']['timer']
    yi = y0+((y1-y0)/resol)*scene.objects['Task']['timer']
    zi = z0+((z1-z0)/resol)*scene.objects['Task']['timer']
    scene.objects['Task'].worldPosition = [xi, yi, zi]
    scene.objects['Task']['timer']+=1
    if scene.objects['Task']['timer']== resol:
        scene.objects['Task']['anim_open'] = False
        scene.objects['Task_close-cmd'].setVisible(True,True)
        scene.objects['Task_close-cmd-Hl'].setVisible(False,True)
        scene.objects['Task_close-cmd'].restorePhysics()

##
# Allumer la tablette des tâches
##

def  task_close ():
    scene.objects['Commands']['task']=False
    scene.objects['Task_close-cmd'].setVisible(False,True)
    scene.objects['Task_close-cmd'].suspendPhysics()
    scene.objects['Task_close-cmd-Hl'].setVisible(False,True)
    scene.objects['Task']['timer'] = 0
    scene.objects['Task']['anim_close'] = True

def task_close_anim():
    x0_cam=0.005783
    y0_cam=-26.4403
    z0_cam=20.2232
    x_cam=scene.objects['Camera'].worldPosition.x
    y_cam=scene.objects['Camera'].worldPosition.y
    z_cam=scene.objects['Camera'].worldPosition.z
    resol=50
    x0 = 0.201271-x0_cam+x_cam
    y0 =-25.9416-y0_cam+y_cam
    z0 = 19.793-z0_cam+z_cam
    x1=0.291678-x0_cam+x_cam
    y1 =-25.9416-y0_cam+y_cam
    z1 = 19.793-z0_cam+z_cam
    xi = x0+((x1-x0)/resol)*scene.objects['Task']['timer']
    yi = y0+((y1-y0)/resol)*scene.objects['Task']['timer']
    zi = z0+((z1-z0)/resol)*scene.objects['Task']['timer']
    scene.objects['Task'].worldPosition = [xi, yi, zi]
    scene.objects['Task']['timer']+=1
    if scene.objects['Task']['timer']== resol:
        scene.objects['Task']['anim_close'] = False
        scene.objects['Task'].setVisible(False,True)
        scene.objects['Task-cmd'].setVisible(True,True)
        scene.objects['Task-cmd-Hl'].setVisible(False,True)
        scene.objects['Task-cmd'].restorePhysics()

##
# Clic pour fermer la tablette des tâches
##

def task_close_click(cont):
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive :
        sound_play (snd_close)
        task_close()

###############################################################################
# Store
###############################################################################

##
# Prendre en compte les upgrades
##

def  upgrade_maj():

    # Read config (upgrades choisis : data/upgrade/ -> [2][i].text)
    upgrade_card=("battery", "beacon", "paint", "speed")
    scene.objects['Points']['upgrade_nb'] =0
    for i in range(len(upgrade_card)):
        if rp_config_tree[2][i].text == "True":
            scene.objects['Points']['upgrade_'+upgrade_card[i]]=True
            scene.objects['Points']['upgrade_nb'] +=1
            scene.objects["Store-"+upgrade_card[i]+"-card"]['upgraded'] = True
    scene.objects['Points']['upgrade_credit']= scene.objects['Points']['level']- 1 - scene.objects['Points']['upgrade_nb']

    # Batterie
    if scene.objects['Points']['upgrade_battery'] :
        scene.objects['St-Panels-3'].setVisible(True,True)
    else:
        scene.objects['St-Panels-3'].setVisible(False,True)

    # Balise, voir rp_marquer ()
    if scene.objects['Points']['upgrade_beacon'] :
        scene.objects['St-Cubes'].setVisible(True,True)
    else:
        scene.objects['St-Cubes'].setVisible(False,True)

    # Peinture (uniquement sous forme de fonction)
    
    # Vitesse
    if scene.objects['Points']['upgrade_speed'] :
        scene.objects['Text_speed'].setVisible(True,True)
        scene.objects['Text_speed_label-fr'].setVisible(True,True)
        scene.objects['Speed_down'].setVisible(True,True)
        scene.objects['Speed_down']. restorePhysics()
        scene.objects['Speed_up'].setVisible(True,True)
        scene.objects['Speed_up']. restorePhysics()
    else:
        scene.objects['Text_speed'].setVisible(False,True)
        scene.objects['Text_speed_label-fr'].setVisible(False,True)
        scene.objects['Speed_down'].setVisible(False,True)
        scene.objects['Speed_down'].suspendPhysics()
        scene.objects['Speed_up'].setVisible(False,True)
        scene.objects['Speed_up'].suspendPhysics()
   
##
# Ouvrir le store
##

def  store_open ():
    upgrade_maj()
    if scene.objects['Grid-u'].visible:
        scene.objects['Store']['Grid_visible'] = True
        terrain_grid()
    else:
        scene.objects['Store']['Grid_visible'] = False
    rp_store.open()

##
# Fermer le store
##

def store_close():
    rp_store.close()
    scene.objects['Terrain']['manip_mode']=0 # Enlever la fenêtre modale

    # Maj du fichier de config (upgrade : data/upgrade/i -> [2][i].text)
    upgrade_card=("battery", "beacon", "paint", "speed")
    for i in range(len(upgrade_card)):
        if  scene.objects["Store-"+upgrade_card[i]+"-card"]['upgraded'] == True:
            rp_config_tree[2][i].text="True"
        else:
            rp_config_tree[2][i].text="False"
    buffer_xml = ET.tostring(rp_config_tree)
    with open("rp_config.xml", "wb") as f:
        f.write(buffer_xml)

    # Overlay
    scene.objects['Points'].setVisible(True,True)
    if scene.objects['Commands']['twins']:
        scene.objects['Points-Twins'].setVisible(True,True)
        scene.objects['Points-Twins-text'].setVisible(True,False)
    else:
        scene.objects['Points-Twins'].setVisible(False,True)
        scene.objects['Points-Twins-text'].setVisible(False,False)
    scene.objects['Commands'].setVisible(True,True)
    scene.objects['Camera'].setVisible(True,True)
    scene.active_camera = scene.objects["Camera"]
    scene.addOverlayCollection(scene.cameras['Camera-Hud'], bpy.data.collections['Hud'])

    # UI : Commands
    scene.objects['Run-Hl'].setVisible(False,False)
    scene.objects['Pause'].setVisible(False,False)
    scene.objects['Pause'].suspendPhysics()
    scene.objects['Pause-Hl'].setVisible(False,False)
    # scene.objects['Stop'].setVisible(False,False)
    # scene.objects['Stop'].suspendPhysics()
    scene.objects['Stop-Hl'].setVisible(False,False)
    scene.objects['Aim-cmd-Hl'].setVisible(False,False)
    scene.objects['Doc-cmd-Hl'].setVisible(False,False)
    scene.objects['ResetView-Hl'].setVisible(False,False)
    scene.objects['About-cmd-Hl'].setVisible(False,False)
    scene.objects['Speed_up-Hl'].setVisible(False,False)
    scene.objects['Speed_down-Hl'].setVisible(False,False)
    scene.objects['Cmd-text'].setVisible(False,False)

    # Task panel
    if scene.objects['Commands']['task']:
        scene.objects['Task-cmd'].setVisible(False,True)
        scene.objects['Task-cmd'].suspendPhysics()
        scene.objects['Task-cmd-Hl'].setVisible(False,True)
        scene.objects['Task_close-cmd'].setVisible(True,True)
        scene.objects['Task_close-cmd'].restorePhysics()
        scene.objects['Task_close-cmd-Hl'].setVisible(False,True)
    else:
        scene.objects['Task_close-cmd'].setVisible(False,True)
        scene.objects['Task_close-cmd'].suspendPhysics()
        scene.objects['Task_close-cmd-Hl'].setVisible(False,True)
        scene.objects['Task-cmd'].setVisible(True,True)
        scene.objects['Task-cmd'].restorePhysics()
        scene.objects['Task-cmd-Hl'].setVisible(False,True)


    # Maj de l'interface
    upgrade_maj()

    # Affichage de la grille + mission
    if scene.objects['Store']['Grid_visible']:
        terrain_grid()

##
# Clic pour fermer le store
##

def store_close_click(cont):
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive :
        sound_play (snd_close)
        store_close()

###############################################################################
# About
###############################################################################

##
# Ouvrir le about
##

def about_open():
    scene.objects['Terrain']['manip_mode']=9 # Fenêtre modale About
    scene.objects['Camera']['current_lx'] = scene.objects['Camera'].worldPosition.x
    scene.objects['Camera']['current_ly'] = scene.objects['Camera'].worldPosition.y
    scene.objects['Camera']['current_lz'] = scene.objects['Camera'].worldPosition.z
    scene.objects['Camera'].worldPosition.x = scene.objects['Camera']['init_lx']
    scene.objects['Camera'].worldPosition.y = scene.objects['Camera']['init_ly']
    scene.objects['Camera'].worldPosition.z = scene.objects['Camera']['init_lz']
    applyRotationTo(scene.objects['Terrain'], 0, 0, 0)
    scene.objects['Cmd-text']['Text']= ""
    rp_about.open()
    
##
# Fermer le about
##

def about_close():
    scene.objects['Terrain']['manip_mode']=0
    scene.objects['About'].setVisible(False,True)
    scene.objects['About'].worldPosition = [42, -2, 3]
    scene.objects['Camera'].worldPosition.x = scene.objects['Camera']['current_lx']
    scene.objects['Camera'].worldPosition.y = scene.objects['Camera']['current_ly']
    scene.objects['Camera'].worldPosition.z = scene.objects['Camera']['current_lz']

##
# Click pour fermer le about
##

def about_close_click(cont):
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive :
        sound_play (snd_close)
        about_close()

##
# Configuration de l'écran
##

def get_near_pos(array,value):
    array = np.asarray(array)
    idx = (np.abs(array-value)).argmin()
    return idx  

def about_screen_up(cont):
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive :
        screen_width_mode=[640, 960, 1280, 1920]
        screen_height_mode=[360, 540, 720,1080]
        screen_mode_txt=["640x360", "960x540", "1280x720", "1920x1080"]
        i = get_near_pos(screen_width_mode, bge.render.getWindowWidth())
        if  i>=0 and i<3 :
            screen_width=screen_width_mode[i+1]
            screen_height=screen_height_mode[i+1]
            scene.objects['About_screen']['Text']= "SCREEN SIZE : "+str(screen_width) +" x "+str(screen_height)
            bge.render.setWindowSize(screen_width,screen_height)

            # Boutons < et >
            if screen_width <=640:
                scene.objects['About_screen-down'].setVisible(False,True)
                scene.objects['About_screen-down-colbox'].suspendPhysics (True)
            else:
                scene.objects['About_screen-down'].setVisible(True,True)
                scene.objects['About_screen-down-colbox'].restorePhysics()
            if screen_width >= 1920:
                scene.objects['About_screen-up'].setVisible(False,True)
                scene.objects['About_screen-up-colbox'].suspendPhysics (True)
            else:
                scene.objects['About_screen-up'].setVisible(True,True)
                scene.objects['About_screen-up-colbox'].restorePhysics()

            # Maj du fichier de config (screen size : data/config/screen/width-> [0][3][0].text)
            rp_config_tree[0][3][0].text=str(screen_width)
            rp_config_tree[0][3][1].text=str(screen_height)
            buffer_xml = ET.tostring(rp_config_tree)
            with open("rp_config.xml", "wb") as f:
                f.write(buffer_xml)

def about_screen_down(cont):
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive :
        screen_width_mode=[640, 960, 1280, 1920]
        screen_height_mode=[360, 540, 720,1080]
        screen_mode_txt=["640x360", "960x540", "1280x720", "1920x1080"]
        i = get_near_pos(screen_width_mode, bge.render.getWindowWidth())
        if  i>0 and i<=3 :
            screen_width=screen_width_mode[i-1]
            screen_height=screen_height_mode[i-1]
            scene.objects['About_screen']['Text']= "SCREEN SIZE : "+str(screen_width) +" x "+str(screen_height)
            bge.render.setWindowSize(screen_width,screen_height)

            # Boutons < et >
            if screen_width <=640:
                scene.objects['About_screen-down'].setVisible(False,True)
                scene.objects['About_screen-down-colbox'].suspendPhysics (True)
            else:
                scene.objects['About_screen-down'].setVisible(True,True)
                scene.objects['About_screen-down-colbox'].restorePhysics()
            if screen_width >= 1920:
                scene.objects['About_screen-up'].setVisible(False,True)
                scene.objects['About_screen-up-colbox'].suspendPhysics (True)
            else:
                scene.objects['About_screen-up'].setVisible(True,True)
                scene.objects['About_screen-up-colbox'].restorePhysics()

            # Maj du fichier de config (screen size : data/config/screen/width-> [0][3][0].text)
            rp_config_tree[0][3][0].text=str(screen_width)
            rp_config_tree[0][3][1].text=str(screen_height)
            buffer_xml = ET.tostring(rp_config_tree)
            with open("rp_config.xml", "wb") as f:
                f.write(buffer_xml)

