import bge # Bibliothèque Blender Game Engine (UPBGE)
import webbrowser

###############################################################################
# rp_about.py
# @title: A propos pour Ropy
# @project: Ropy (Blender-EduTech)
# @lang: fr
# @authors: Philippe Roy <philippe.roy@ac-grenoble.fr>
# @copyright: Copyright (C) 2022 Philippe Roy
# @license: GNU GPL
###############################################################################

scene = bge.logic.getCurrentScene()

# Colors
color_link =  (0, 1, 0.857,1) # Turquoise
color_link_hl =  (0.799, 0.617, 0.021, 1) # Jaune

# UPBGE constants
JUST_ACTIVATED = bge.logic.KX_INPUT_JUST_ACTIVATED
JUST_RELEASED = bge.logic.KX_INPUT_JUST_RELEASED
ACTIVATE = bge.logic.KX_INPUT_ACTIVE
# JUST_DEACTIVATED = bge.logic.KX_SENSOR_JUST_DEACTIVATED

##
# Ouverture
##

def open():
    scene.objects['About_close'].color= color_link
    scene.objects['About_screen-up'].color= color_link
    scene.objects['About_screen-down'].color= color_link
    scene.objects['About_screen']['Text']= "SCREEN SIZE : "+str(bge.render.getWindowWidth()) +" x "+str(bge.render.getWindowHeight())

    scene.objects['About_link-git'].color= color_link
    scene.objects['About_link-gpl'].color= color_link
    scene.objects['About_link-upbge'].color= color_link
    scene.objects['About_link-kay'].color= color_link
    scene.objects['About_link-kenney'].color= color_link
    scene.objects['About_link-polygonrunway'].color= color_link
    scene.objects['About_link-icons'].color= color_link
    
    scene.objects['About'].setVisible(True,True)
    scene.objects['About'].worldPosition = [0, 1.53623, -1.8]     # old [0, 1.53623, -0.892838]
    scene.objects['About']['timer'] = 0
    scene.objects['About']['anim'] = True

    # Boutons < et > ("640x360", "960x540", "1280x720", "1920x1080")
    if bge.render.getWindowWidth() <=640:
        scene.objects['About_screen-down'].setVisible(False,True)
        scene.objects['About_screen-down-colbox'].suspendPhysics (True)
    else:
        scene.objects['About_screen-down'].setVisible(True,True)
        scene.objects['About_screen-down-colbox'].restorePhysics()
    if bge.render.getWindowWidth() >= 1920:
        scene.objects['About_screen-up'].setVisible(False,True)
        scene.objects['About_screen-up-colbox'].suspendPhysics (True)
    else:
        scene.objects['About_screen-up'].setVisible(True,True)
        scene.objects['About_screen-up-colbox'].restorePhysics()

##
# Animation de l'ouverture
##

def open_anim():
    resol=0.5
    scene.objects['About'].localPosition.y -= 1.09*resol
    scene.objects['About'].localPosition.z += 0.85*resol
    scene.objects['About']['timer']+=1
    if scene.objects['About']['timer']== 40:
        scene.objects['About']['anim'] = False

##
# Highlight
##

def hl(cont):
    if cont.sensors['MO'].status == JUST_ACTIVATED:
        obj = cont.owner
        name=obj.name[:-7]
        scene.objects[name].color = color_link_hl

    if cont.sensors['MO'].status == JUST_RELEASED:
        obj = cont.owner
        name=obj.name[:-7]
        scene.objects[name].color = color_link

##
# Liens
##

def link(cont):
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive :
        obj = cont.owner
        name=obj.name[:-7]
        link={
            'About_link-git' : 'https://gitlab.com/blender-edutech/ropy',
            'About_link-gpl' : 'https://www.gnu.org/licenses/gpl-3.0.html',
            'About_link-upbge' : 'https://www.upbge.org',
            'About_link-kay' : 'https://www.kaylousberg.com',
            'About_link-kenney' : 'https://www.kenney.nl',
            'About_link-polygonrunway' : 'https://www.patreon.com/polygonrunway/',
            'About_link-icons' : 'https://game-icons.net/'}
        webbrowser.open(link [name])

def link_hl(cont):
    decal = 15
    if cont.sensors['MO'].status == JUST_ACTIVATED :
        obj = cont.owner
        name=obj.name[:-7]
        scene.objects[name].color = color_link_hl

    if cont.sensors['MO'].status == JUST_RELEASED :
        obj = cont.owner
        name=obj.name[:-7]
        scene.objects[name].color = color_link
