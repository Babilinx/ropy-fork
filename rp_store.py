from rp_lib import * # Bibliothèque Ropy
import rp_doc # Documentation

###############################################################################
# rp_store.py
# @title: Magasin pour le Rover Ropy
# @project: Ropy (Blender-EduTech)
# @lang: fr
# @authors: Philippe Roy <philippe.roy@ac-grenoble.fr>
# @copyright: Copyright (C) 2022 Philippe Roy
# @license: GNU GPL
# 
# Ropy est destiné à la découverte de la programmation procédurale et du language Python.
# A travers plusieurs challenges, donc de manière graduée, les élèves vont apprendre à manipuler les structures algorithmiques de base et à les coder en Python.
###############################################################################

scene = bge.logic.getCurrentScene()

# Colors
color_store_fct =  (0, 1, 0.857,1) # Turquoise
color_store_hl =  (0.799, 0.617, 0.021, 1) # Jaune
color_store_activate =  (0.936, 0.033, 1, 1) # Rose
color_store_upgraded =  (1, 0.192, 0.03, 1) # Orange

# UPBGE constants
JUST_ACTIVATED = bge.logic.KX_INPUT_JUST_ACTIVATED
JUST_RELEASED = bge.logic.KX_INPUT_JUST_RELEASED
ACTIVATE = bge.logic.KX_INPUT_ACTIVE
# JUST_DEACTIVATED = bge.logic.KX_SENSOR_JUST_DEACTIVATED

################################################################################
# Init, open et close
################################################################################

##
# Initialisation de la tablette
##

def init():

    # Mémorisation de la position de la tablette du store
    scene.objects["Store"]['init_lx']=scene.objects["Store"].worldPosition.x
    scene.objects["Store"]['init_ly']=scene.objects["Store"].worldPosition.y
    scene.objects["Store"]['init_lz']=scene.objects["Store"].worldPosition.z

    # Placement de la position de la tablette du store
    scene.objects['Store'].worldScale = [0.01, 0.01, 0.01]
    # applyRotationTo(scene.objects['Store'], None, (-0.2*math.pi)/180, (115*math.pi)/180, False)
    applyRotationTo(scene.objects['Store'], None, (-2.38*math.pi)/180, (100*math.pi)/180, False) # dernière version
    # scene.objects['Store'].applyRotation((math.pi/2+(0.59189*math.pi)/180, 0, 0), True)
    # scene.objects['Store'].applyRotation((math.pi/4+(2*math.pi)/180, 0, 0), True) # avant dernière version
    scene.objects['Store'].applyRotation((math.pi/2+(3*math.pi)/180, 0, 0), True) # dernière version

##
# Ouverture du store
##

def open ():
    scene.objects['Rover']['store-anim_end']=False
    scene.objects['Rover']['stop']=False
    thread_gostore_start(rover_go_store) # Aller au store

##
# Arrivée au store
##

def open2 (cont):
    obj=scene.objects['Rover']
    thread_gostore_stop()
    obj['store-anim_end']=False

    # Pas arrivé au store, dommage !
    if obj['stop']:
        return False

    # Affinage de la position
    obj.worldPosition.x = -9.75
    obj.worldPosition.y = -4
    obj.worldPosition.z = 0.19

    # Overlay
    scene.objects['Terrain']['manip_mode']=8 # Fenêtre modale type Doc
    scene.removeOverlayCollection(bpy.data.collections['Hud'])
    scene.objects['Points'].setVisible(False,True)
    scene.objects['Commands'].setVisible(False,True)
    scene.active_camera = scene.objects["Camera-Store"]
    scene.objects['Camera'].setVisible(False,True)

    # Animation de la bulle
    scene.objects['Bubble-1-text']['Text']="Salut mon ami !\n Souhaites tu\n acquerir des\n nouvelles\n ameliorations ?\n"
    if scene.objects['Points']['upgrade_nb'] == 4 : 
        scene.objects['Bubble-1-text']['Text']="Resalut ! \n Rien de neuf \n pour le moment. \n Il fait beau \n aujourd'hui, non ?"
    if scene.objects['Points']['upgrade_nb'] < 4  and scene.objects['Points']['upgrade_credit']==0:
        scene.objects['Bubble-1-text']['Text']="Des missions \n tu passeras, \n des crédits \n tu auras !"
       
    start = 1
    end = 20
    layer = 0
    priority = 1
    blendin = 1.0
    mode = bge.logic.KX_ACTION_MODE_PLAY
    layerWeight = 0.0
    ipoFlags = 0
    # speed = scene.objects['Commands']['speed']*4
    speed = 0.5
    scene.objects['Bubble-1'].setVisible(True,True)
    # scene.objects['Bubble-1-line'].playAction('Bubble-1-line-ShaderAction', start, end, layer, priority, blendin, mode, layerWeight, ipoFlags, speed)
    # scene.objects['Bubble-1-text'].setVisible(True,True)
    scene.objects['Bubble-1-text'].playAction('Bubble-1-text-ShaderAction', start, end, layer, priority, blendin, mode, layerWeight, ipoFlags, speed)

    # Mise en couleurs
    scene.objects['Store_close'].color= color_store_fct
    scene.objects['Store_upgrade'].color= color_store_fct
    scene.objects['Store_upgrade'].setVisible(False, True)
    scene.objects['Store_upgrade-colbox'].suspendPhysics (True)
    upgrade_card=("battery", "beacon", "paint", "speed")
    for i in range(len(upgrade_card)):
        if scene.objects["Store-"+upgrade_card[i]+"-card"]['upgraded'] == True:
            scene.objects["Store-"+upgrade_card[i]+"-card"].color = color_store_upgraded
            scene.objects["Store-"+upgrade_card[i]+"-card-icon"].color = color_store_upgraded
            scene.objects["Store-"+upgrade_card[i]+"-card-text"].color = color_store_upgraded
        else:
            scene.objects["Store-"+upgrade_card[i]+"-card"].color = color_store_fct
            scene.objects["Store-"+upgrade_card[i]+"-card-icon"].color = color_store_fct
            scene.objects["Store-"+upgrade_card[i]+"-card-text"].color = color_store_fct
    scene.objects['Store']['page_fct'] =""
    scene.objects['Store_text']['Text'] = " "
    scene.objects['Store_credits']['Text'] = "Crédits : "+str(scene.objects['Points']['upgrade_credit'])
        
    # Animation de la tablette
    scene.objects['Store'].setVisible(True,True)
    scene.objects['Store'].worldPosition = [-12.25, -2.7, 0.4]
    scene.objects['Store']['timer'] = 0
    scene.objects['Store']['anim'] = True
    scene.objects['Store_upgrade'].setVisible(False, True)
    scene.objects['Store_upgrade-colbox'].suspendPhysics (True)
    scene.objects['Store_text'].setVisible(False,True)

##
# Animation du store
##

def open_anim():
    resol=100
    x0 = -10.1722
    y0 = -2.68957
    z0 =0.42148
    x1 = -9.05968
    y1 = -5.01656
    z1= 0.521224
    xi = x0+((x1-x0)/resol)*scene.objects['Store']['timer']
    yi = y0+((y1-y0)/resol)*scene.objects['Store']['timer']
    zi = z0+((z1-z0)/resol)*scene.objects['Store']['timer']
    scale0=0.01
    # scale1=0.169
    scale1=0.2
    scalei = scale0+((scale1-scale0)/resol)*scene.objects['Store']['timer']
    scene.objects['Store'].worldPosition = [xi, yi, zi]
    scene.objects['Store'].worldScale = [scalei, scalei, scalei]
    scene.objects['Store']['timer']+=1
    if scene.objects['Store']['timer']== resol:
        scene.objects['Store']['anim'] = False

##
# Fermeture
##

def close ():
    scene.objects["Store"].worldPosition.x=scene.objects["Store"]['init_lx']
    scene.objects["Store"].worldPosition.y=scene.objects["Store"]['init_ly']
    scene.objects["Store"].worldPosition.z=scene.objects["Store"]['init_lz']
    scene.objects['Store'].setVisible(False,True)
    scene.objects['Bubble-1'].setVisible(False,True)
    # scene.objects['Bubble-1-text'].setVisible(False,True)

###############################################################################
# Interface
###############################################################################

##
# Highlight du store
##

def  hl (cont):

    # Activation
    if cont.sensors['MO'].status == JUST_ACTIVATED :
        obj = cont.owner
        name=obj.name[:-7]
        name_text=name+"-text"
        name_icon=name+"-icon"
        scene.objects[name].color = color_store_hl
        
        # Close et button
        if name == "Store_close" or name == "Store_upgrade":
            scene.objects[name].color = color_store_hl
        else:
            scene.objects[name].color = color_store_hl
            scene.objects[name_text].color = color_store_hl
            scene.objects[name_icon].color = color_store_hl

    # Désactivation
    if cont.sensors['MO'].status == JUST_RELEASED :
        obj = cont.owner
        name=obj.name[:-7]
        name_text=obj.name[:-7]+"-text"
        name_icon=obj.name[:-7]+"-icon"

        # Close et button
        if name == "Store_close" or name == "Store_upgrade":
            scene.objects[name].color = color_store_fct
        else:
            if name == scene.objects['Store']['page_fct'] :
                scene.objects[name].color = color_store_activate
                scene.objects[name_text].color = color_store_activate
                scene.objects[name_icon].color = color_store_activate
            else:
                if scene.objects[name]['upgraded'] == True:
                    scene.objects[name].color = color_store_upgraded
                    scene.objects[name_text].color = color_store_upgraded
                    scene.objects[name_icon].color = color_store_upgraded
                else:
                    scene.objects[name].color = color_store_fct
                    scene.objects[name_text].color = color_store_fct
                    scene.objects[name_icon].color = color_store_fct

##
# Afficher les details de la fonction à partir d'une carte
##

def card (cont):
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive :
        obj = cont.owner
        name_fct= obj.name[:-7]
        scene.objects['Store_text']['Text'] = " "

        # Enlever l'ancienne carte
        if scene.objects['Store']['page_fct'] !="":
            if scene.objects[scene.objects['Store']['page_fct']]['upgraded'] == True:
                scene.objects[scene.objects['Store']['page_fct']].color = color_store_upgraded
                scene.objects[scene.objects['Store']['page_fct']+'-text'].color = color_store_upgraded
                scene.objects[scene.objects['Store']['page_fct']+'-icon'].color = color_store_upgraded
            else:
                scene.objects[scene.objects['Store']['page_fct']].color = color_store_fct
                scene.objects[scene.objects['Store']['page_fct']+'-text'].color = color_store_fct
                scene.objects[scene.objects['Store']['page_fct']+'-icon'].color = color_store_fct

        # Afficher le texte de la carte
        scene.objects['Store']['page_fct'] = name_fct
        scene.objects[name_fct].color = color_store_activate
        scene.objects[name_fct+'-icon'].color = color_store_activate
        scene.objects[name_fct+'-text'].color = color_store_activate
        scene.objects['Store_text']['Text'] = rp_doc.upgrade_description(name_fct)
        scene.objects['Store_text'].setVisible(True, False)

        # Upgrade
        if scene.objects[name_fct]['upgraded'] == True or scene.objects['Points']['upgrade_credit'] ==0:
            scene.objects['Store_upgrade'].setVisible(False, False)
            scene.objects['Store_upgrade-colbox'].suspendPhysics (True)
        else:
            scene.objects['Store_upgrade'].setVisible(True, False)
            scene.objects['Store_upgrade-colbox']. restorePhysics()

##
# Selectionner l'upgrade
##

def upgrade (cont):
    if cont.sensors['Click'].status == JUST_ACTIVATED and cont.sensors['MO'].positive :
        # sound_play (sndbuff_book_flip)
        obj = cont.owner
        name_fct = scene.objects['Store']['page_fct']
        if name_fct !="":
            scene.objects[name_fct]['upgraded'] = True
            scene.objects[scene.objects['Store']['page_fct']].color = color_store_upgraded
            scene.objects[scene.objects['Store']['page_fct']+'-text'].color = color_store_upgraded
            scene.objects[scene.objects['Store']['page_fct']+'-icon'].color = color_store_upgraded
            scene.objects['Points']['upgrade_nb'] +=1
            scene.objects['Points']['upgrade_credit']= scene.objects['Points']['level']- 1 - scene.objects['Points']['upgrade_nb']
            scene.objects['Store_credits']['Text'] = "Crédits : "+str(scene.objects['Points']['upgrade_credit'])
            scene.objects['Store_upgrade'].setVisible(False, False)
            scene.objects['Store_upgrade'].suspendPhysics (True)
            scene.objects['Bubble-1-text']['Text']=rp_doc.upgrade_talk(name_fct)

###############################################################################
# Rover
###############################################################################

##
# Direction
##

def rover_dir (direction):
    obj=scene.objects['Rover']
    step=math.pi/2 # Pas angulaire
    # print ("rover_dir : ",direction)
    
    # Nord
    if direction=="n":
            if round(obj.worldOrientation.to_euler().z, 2) == 0.00:  # Sud
                rp_gauche()
                rp_gauche()
            elif round(obj.worldOrientation.to_euler().z, 2) == round(math.pi,2) or round(obj.worldOrientation.to_euler().z, 2) == - round(math.pi,2) :  # Nord
                pass
            elif round(obj.worldOrientation.to_euler().z, 2) == round(math.pi/2,2) or round(obj.worldOrientation.to_euler().z, 2) == -round(3*(math.pi/2),2) :  # Est
                rp_gauche()
            elif round(obj.worldOrientation.to_euler().z, 2) == round(-math.pi/2,2) or round(obj.worldOrientation.to_euler().z, 2) == round(3*(math.pi/2),2) :  # Ouest
                rp_droite()

    # Sud
    if direction=="s":
            if round(obj.worldOrientation.to_euler().z, 2) == 0.00:  # Sud
                pass
            elif round(obj.worldOrientation.to_euler().z, 2) == round(math.pi,2) or round(obj.worldOrientation.to_euler().z, 2) == - round(math.pi,2) :  # Nord
                rp_gauche()
                rp_gauche()
            elif round(obj.worldOrientation.to_euler().z, 2) == round(math.pi/2,2) or round(obj.worldOrientation.to_euler().z, 2) == -round(3*(math.pi/2),2) :  # Est
                rp_droite()
            elif round(obj.worldOrientation.to_euler().z, 2) == round(-math.pi/2,2) or round(obj.worldOrientation.to_euler().z, 2) == round(3*(math.pi/2),2) :  # Ouest
                rp_gauche()

    # Est
    if direction=="e":
            if round(obj.worldOrientation.to_euler().z, 2) == 0.00:  # Sud
                rp_gauche()
            elif round(obj.worldOrientation.to_euler().z, 2) == round(math.pi,2) or round(obj.worldOrientation.to_euler().z, 2) == - round(math.pi,2) :  # Nord
                rp_droite()
            elif round(obj.worldOrientation.to_euler().z, 2) == round(math.pi/2,2) or round(obj.worldOrientation.to_euler().z, 2) == -round(3*(math.pi/2),2) :  # Est
                pass
            elif round(obj.worldOrientation.to_euler().z, 2) == round(-math.pi/2,2) or round(obj.worldOrientation.to_euler().z, 2) == round(3*(math.pi/2),2) :  # Ouest
                rp_gauche()
                rp_gauche()

    # Ouest
    if direction=="o":
            if round(obj.worldOrientation.to_euler().z, 2) == 0.00:  # Sud
                rp_droite()
            elif round(obj.worldOrientation.to_euler().z, 2) == round(math.pi,2) or round(obj.worldOrientation.to_euler().z, 2) == - round(math.pi,2) :  # Nord
                rp_gauche()
            elif round(obj.worldOrientation.to_euler().z, 2) == round(math.pi/2,2) or round(obj.worldOrientation.to_euler().z, 2) == -round(3*(math.pi/2),2) :  # Est
                rp_gauche()
                rp_gauche()
            elif round(obj.worldOrientation.to_euler().z, 2) == round(-math.pi/2,2) or round(obj.worldOrientation.to_euler().z, 2) == round(3*(math.pi/2),2) :  # Ouest
                pass

##
# Avancer pas
##

def rover_avancer_nbpas(pas):
    for i in range (int(pas)):
        rp_avancer()

def rover_dir_nbpas(direction, pas):
    if pas>0:
        rover_dir (direction)
        rover_avancer_nbpas(pas)

##
# Pathfinder pour le store
##

def rover_go_store():
    obj=scene.objects['Rover']
    scene.objects['Points']['step'] =0

    # Position de départ pour le debug
    # obj.worldPosition.x=12
    # obj.worldPosition.y=9
    x0 = obj.worldPosition.x
    y0 = obj.worldPosition.y
    z0 = obj.worldPosition.z
    # print ("Position actuelle :", x0, y0)

    # Pathfinder : zone 1
    if  x0<=-10 and (y0==2 or y0==3):
        # print ("Store : zone 1 -> -8;4")
        rover_dir_nbpas("n",4-y0)
        rover_dir_nbpas("e",-x0-8)
        x0 = obj.worldPosition.x
        y0 = obj.worldPosition.y
        # print ("Position actuelle :", x0, y0)

    # Pathfinder : zone 2
    if  x0<=-9 and y0>=4:
        # print ("Store : zone 2 -> -8;4")
        rover_dir_nbpas("e",-x0-8)
        rover_dir_nbpas("s",y0-4)
        x0 = obj.worldPosition.x
        y0 = obj.worldPosition.y
        # print ("Position actuelle :", x0, y0)

    # Pathfinder : zone 3
    if  x0==-9 and y0==1:
        # print ("Store : zone 3 -> -8;4")
        rover_dir_nbpas("e",1)
        x0 = obj.worldPosition.x
        y0 = obj.worldPosition.y
        # print ("Position actuelle :", x0, y0)

    # Pathfinder : zone 4
    if  x0>=14 and y0<=-1:
        # print ("Store : zone 4 -> x;0")
        rover_dir_nbpas("n",-y0)
        x0 = obj.worldPosition.x
        y0 = obj.worldPosition.y
        # print ("Position actuelle :", x0, y0)
  
    # Pathfinder : zone 5
    if  x0>=-8 and y0>=0:
        # print ("Store : zone 5 -> -8;0 puis -8;-1")
        rover_dir_nbpas("s",y0)
        rover_dir_nbpas("o",8+x0)
        rover_dir_nbpas("s",1)
        x0 = obj.worldPosition.x
        y0 = obj.worldPosition.y
        # print ("Position actuelle :", x0, y0)

    # Pathfinder : zone 6
    if  y0>=-4:
        # print ("Store : zone 6 -> -10;-4")
        rover_dir_nbpas("s",y0+4)
        rover_dir_nbpas("o",10+x0)
        x0 = obj.worldPosition.x
        y0 = obj.worldPosition.y
        # print ("Position actuelle :", x0, y0)

    # Pathfinder : zone 7
    if  x0<=-11 and y0<=-5:
        # print ("Store : zone 7 -> -10;y")
        rover_dir_nbpas("e",-x0-10)
        x0 = obj.worldPosition.x
        y0 = obj.worldPosition.y
        # print ("Position actuelle :", x0, y0)

    # Pathfinder : zone 8
    if  y0<=-5:
        # print ("Store : zone 8 -> -10;-4")
        rover_dir_nbpas("n",-y0-4)
        rover_dir_nbpas("o",10+x0)
        x0 = obj.worldPosition.x
        y0 = obj.worldPosition.y
        # print ("Position actuelle :", x0, y0)

    rover_dir ("o")
    obj['store-anim_end']=True
